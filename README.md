# A'Maze'ng FX Controls

---

A program that generates and solves mazes.

The JavaFX graphical demo program provides the animation of the progress of path searching

Made By Hans Zuidervaart. inspired by python project from Braidy (ComputerPhile).

![maze](src/main/resources/input/A'Maze'ng.png)

---

## Setup

Before you run the program, make sure you modify the following in Intellij:

- Go to File -->  Settings --> Appearance and Behaviour --> Path Variables
- Add a new path variable by clicking on the + sign and name it PATH_TO_FX and browse to the lib folder of the JavaFX SDK to set its value, and click apply.
- Go to Run --> Edit Configurations
- Type the following in the VM Options section: --module-path ${PATH_TO_FX} --add-modules javafx.controls,javafx.fxml
- Click Apply and Run the application. It should work now.

- You can add the following command in the Run -> vm options line to increase the heap size: -Xmx2048m
  The number indicates how much memory is allocated?

source: https://openjfx.io/openjfx-docs/#IDE-Intellij

---

## About

The goal of this project was to find a nice way to implemented the Dijkstra shortest path algorithm.
The Dijkstra shortest path algorithm or A*, a slight improvement on the Dijkstra Algorithm is used in every satNav all over the world.
It is also implemented in many other fields where the shortest path in a graph has to be found.

This is an example of the user interface interface:
![example1](src/main/resources/documentation/example1.png)

#####Dijkstra's Algorithm
Bellow are the steps that are implemented by the Dijkstra algorithm.
![step1](src/main/resources/documentation/step1.png)

![step2](src/main/resources/documentation/step2.png)

![step8](src/main/resources/documentation/step8.png)

![table](src/main/resources/documentation/table.png)

---

## Input

Some example mazes are included in the repository. They are mostly generated by the generation algorithm included in the project.

Some properties of every input maze:

- All input mazes are surrounded entirely by dark walls.
- One path square exists on the top row of the maze, and is the start of the maze.
- One path square exists on the bottom row of the maze, and is the end of the maze.

---

## Notes

Some things to note:

- The larger mazes use a lot of ram. When trying to solve larger mazes you may run out of memory!
- When the output files don'treasure show in the output folder, then open the output folder in the explorer.
- The current format of the test mazes (short paths, very dense) means that in fact dijkstra and a* usually operate more slowly than simple algorithms. In these cases Dijkstra usually performs the same function as breadth first search, but with more computational overhead.
  There will be some forms of mazes, particularly imperfect maze with many different solutions and where the solution lengths differ much...
  then Dijkstra is significantly faster.
- Mazes don'treasure need to be square - as long as they are surrounded by dark walls.

Enjoy!

---
