package hzt.model.data;

public enum MazeCreatorEnum {

    RANDOM_NEIGHBOUR_PICKER("Random neighbour picker"),
    FIBONACCI_NEIGHBOUR_PICKER("Fibonacci neighbour picker "),
    MODULO_NEIGHBOUR_PICKER("Modulo neighbour picker"),
    VERTICAL_NEIGHBOUR_PICKER("Vertical neighbour picker"),
    HORIZONTAL_NEIGHBOUR_PICKER("Horizontal neighbour picker"),
    SPIRAL_NEIGHBOUR_PICKER("Spiral neighbour picker"),
    TEST_NEIGHBOUR_PICKER("Test neighbour picker");

    private final String name;

    MazeCreatorEnum(String name) {
        this.name = name;
    }

    public java.lang.String getName() {
        return name;
    }
}
