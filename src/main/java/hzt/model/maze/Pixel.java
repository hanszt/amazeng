package hzt.model.maze;

public class Pixel implements Comparable<Pixel> {

    private static int nrOfPixels = 0;
    private final int nr;
    private final int x;
    private final int y;
    private int solutionEntryNr = -1;
    private boolean isPath;
    private boolean isCorner;

    public Pixel(int x, int y, boolean isPath) {
        this.x = x;
        this.y = y;
        this.isPath = isPath;
        this.nr = ++nrOfPixels;
    }

    @Override
    public int compareTo(Pixel other) {
        return this.x - other.getX();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isPath() {
        return isPath;
    }

    public void setPath(boolean path) {
        isPath = path;
    }

    public int getSolutionEntryNr() {
        return solutionEntryNr;
    }

    public void setSolutionEntryNr(int solutionEntryNr) {
        this.solutionEntryNr = solutionEntryNr;
    }

    public boolean isCorner() {
        return isCorner;
    }

    public void setCorner(boolean corner) {
        isCorner = corner;
    }

    @Override
    public String toString() {
        return "Pixel{" +
                "pixel number=" + nr +
                ", x position=" + x +
                ", y position=" + y +
                ", solutionEntry number=" + solutionEntryNr +
                ", isPath=" + isPath +
                ", isCorner=" + isCorner +
                '}';
    }
}
