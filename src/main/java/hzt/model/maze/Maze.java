package hzt.model.maze;

import hzt.model.graph.Graph;
import hzt.model.graph.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.nanoTime;
import static java.lang.System.out;

public class Maze {

    private static int nrOfMazes = 0;

    private final int id;
    private final String name;
    private final Pixel[][] pixels;
    private final Graph graph;

    public Maze(String name, Pixel[][] pixels) {
        this.name = name;
        this.pixels = Arrays.copyOf(pixels, pixels.length);
        this.graph = extractMazeGraph(pixels);
        id = ++nrOfMazes;
    }

    private Graph extractMazeGraph(Pixel[][] graphPixels) {
        double startTime = nanoTime();
        out.println("Maze graph is being created...");
        locateTurnsAndNodes(graphPixels);
        Node[] nodes = getNodes(graphPixels);
        for (Node node : nodes) {
            node.findAdjacentNodes(graphPixels);
        }
        Node startNode = nodes[0];
        Node endNode = nodes[nodes.length - 1];
        out.printf("Maze graph is created in %.3f seconds.\n", (nanoTime() - startTime) / 1e9);
        return new Graph("Graph_of_" + this.getName(), startNode, endNode, nodes);
    }

    private static void locateTurnsAndNodes(Pixel[][] pixels) {
        int xStart = getStartAndEndNodeXPosition(pixels, 0);
        int xEnd = getStartAndEndNodeXPosition(pixels, pixels[0].length - 1);
        // locate beginNode
        pixels[xStart][0] = new Node(xStart, 0);
        // We don't evaluate the outer wall for the rest of the maze
        for (int y = 1; y < pixels[0].length - 1; y++) {
            for (int x = 1; x < pixels.length - 1; x++) {
                if (pixels[x][y].isPath()) {
                    determineIfTurn(pixels, x, y);
                    determineIfNode(pixels, x, y);
                }
            }
        }
        // locate endNode
        pixels[xEnd][pixels[0].length - 1] = new Node(xEnd, pixels[0].length - 1);
    }

    private static void determineIfTurn(Pixel[][] pixels, int x, int y) {
        if (pixels[x - 1][y].isPath() && pixels[x][y - 1].isPath()) {
            pixels[x][y].setCorner(true);
        }
        if (pixels[x + 1][y].isPath() && pixels[x][y - 1].isPath()) {
            pixels[x][y].setCorner(true);
        }
        if (pixels[x - 1][y].isPath() && pixels[x][y + 1].isPath()) {
            pixels[x][y].setCorner(true);
        }
        if (pixels[x + 1][y].isPath() && pixels[x][y + 1].isPath()) {
            pixels[x][y].setCorner(true);
        }
    }

    private static void determineIfNode(Pixel[][] pixels, int x, int y) {
        // determine all junctions and set them as Node
        if (pixels[x - 1][y].isPath() && pixels[x][y + 1].isPath() && pixels[x + 1][y].isPath()) {
            pixels[x][y] = new Node(x, y);
        } else if (pixels[x + 1][y].isPath() && pixels[x][y + 1].isPath() && pixels[x][y - 1].isPath()) {
            pixels[x][y] = new Node(x, y);
        } else if (pixels[x - 1][y].isPath() && pixels[x][y - 1].isPath() && pixels[x][y + 1].isPath()) {
            pixels[x][y] = new Node(x, y);
        } else if (pixels[x + 1][y].isPath() && pixels[x][y - 1].isPath() && pixels[x - 1][y].isPath()) {
            pixels[x][y] = new Node(x, y);
        }
        // determine all dead ends and set them as Node
        else if (!pixels[x - 1][y].isPath() && !pixels[x][y + 1].isPath() && !pixels[x + 1][y].isPath()) {
            pixels[x][y] = new Node(x, y);
        } else if (!pixels[x + 1][y].isPath() && !pixels[x][y + 1].isPath() && !pixels[x][y - 1].isPath()) {
            pixels[x][y] = new Node(x, y);
        } else if (!pixels[x - 1][y].isPath() && !pixels[x][y - 1].isPath() && !pixels[x][y + 1].isPath()) {
            pixels[x][y] = new Node(x, y);
        } else if (!pixels[x + 1][y].isPath() && !pixels[x][y - 1].isPath() && !pixels[x - 1][y].isPath()) {
            pixels[x][y] = new Node(x, y);
        }
    }

    private static int getStartAndEndNodeXPosition(Pixel[][] pixels, int y) {
        for (int x = 0; x < pixels.length; x++) {
            if (pixels[x][y].isPath()) {
                return x;
            }
        }
        return 0;
    }

    private Node[] getNodes(Pixel[][] pixels) {
        List<Node> nodeList = new ArrayList<>();
        for (int y = 0; y < pixels[0].length; y++) {
            for (Pixel[] pixel : pixels) {
                if (pixel[y] instanceof Node node) {
                    nodeList.add(node);
                }
            }
        }
        return nodeList.toArray(new Node[0]);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Pixel[][] getPixels() {
        return Arrays.copyOf(pixels, pixels.length);
    }

    public Graph getGraph() {
        return graph;
    }

    public int getWidth() {
        return pixels.length;
    }

    public int getHeight() {
        return pixels[0].length;
    }

    @Override
    public String toString() {
        return String.format("Number: %d Name: %s, %nMaze width: %d, Maze height: %d nr. of Pixels: %d%n",
                id, name, pixels.length, pixels[0].length, pixels.length * pixels[0].length);
    }
}
