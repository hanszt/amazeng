package hzt.model.graph;

import java.util.Arrays;
import java.util.List;

public class Graph {

    private final String name;
    private final Node startNode;
    private final Node exitNode;
    private final List<Node> allNodes;

    public Graph(String name, Node startNode, Node exitNode, Node... allNodes) {
        this.name = name;
        this.startNode = startNode;
        this.exitNode = exitNode;
        this.allNodes = Arrays.asList(allNodes);
    }

    public Node getStartNode() {
        return startNode;
    }

    public Node getExitNode() {
        return exitNode;
    }

    @Override
    public String toString() {
        return String.format("\n%s \nstartNode: %s \nexitNode: %s \nTotal number of nodes: %d\n",
                name, startNode, exitNode, allNodes.size());
    }
}


