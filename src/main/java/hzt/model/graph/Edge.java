package hzt.model.graph;

import hzt.model.maze.Pixel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Edge implements Comparable<Edge> {

    private static int nrOfEdges = 0;
    private final int id;
    private int weight;
    private final Map<Pixel, Integer> pathToAdjNode;

    public Edge(Map<Pixel, Integer> pathToAdjNode) {
        this.pathToAdjNode = pathToAdjNode;
        this.id = ++nrOfEdges;
        setWeight(pathToAdjNode);
    }

    private void setWeight(Map<Pixel, Integer> entriesToAdjNode) {
        entriesToAdjNode.forEach((key, value) -> this.weight += value);
    }

    public List<Pixel> getEntriesToAdjNode() {
        List<Entry<Pixel, Integer>>entryList = new ArrayList<>(this.pathToAdjNode.entrySet());
        return entryList.stream().map(Entry::getKey).collect(Collectors.toList());
    }

    public Node getAdjNode() {
        List<Entry<Pixel, Integer>>entryList = new ArrayList<>(this.pathToAdjNode.entrySet());
        Entry<Pixel, Integer> lastEntry = entryList.get(entryList.size() - 1);
        return (Node) lastEntry.getKey();
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public int compareTo(Edge other) {
        return this.weight - other.weight;
    }

    @Override
    public String toString() {
        return String.format(" [Edge %d weight: %d numberOfEdgeEntries: %d] \n", id, weight, pathToAdjNode.size());
    }
}
