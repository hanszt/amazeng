package hzt.model.graph;

import hzt.model.maze.Pixel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.lang.Math.abs;

public final class Node extends Pixel {

    private static final int[][] DELTA_MATRIX = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    private final Set<Pixel> visitedPathPixels = new HashSet<>();
    private int cost;
    private final Map<Node, Edge> adjacentNodes;
    private final List<Node> resultPath;

    public Node(int xPosition, int yPosition) {
        super(xPosition, yPosition, true);
        this.cost = Integer.MAX_VALUE;
        this.adjacentNodes = new HashMap<>();
        this.resultPath = new LinkedList<>();
    }

    public List<Edge> getEdges() {
        return List.copyOf(adjacentNodes.values());
    }

    public Map<Node, Edge> getAdjacentNodes() {
        return Map.copyOf(adjacentNodes);
    }

    public void findAdjacentNodes(Pixel[][] graphPixels) {
        for (int[] deltaMatrix : DELTA_MATRIX) {
            Map<Pixel, Integer> pathToAdjNode = findPathToAdjNode(deltaMatrix[0], deltaMatrix[1], graphPixels);
            visitedPathPixels.clear();
            if (!pathToAdjNode.isEmpty()) {
                final var edge = new Edge(pathToAdjNode);
                adjacentNodes.put(edge.getAdjNode(), edge);
            }
        }
    }

    private Map<Pixel, Integer> findPathToAdjNode(int deltaX, int deltaY, Pixel[][] graphPixels) {
        Map<Pixel, Integer> pathToAdjNode = new LinkedHashMap<>();
        Pixel firstEntry = getNextEntry(this, deltaX, deltaY, graphPixels);
        int currentWeight;
        if (firstEntry != null) {
            currentWeight = getWeightToNextEntry(this, firstEntry);
            pathToAdjNode.put(firstEntry, currentWeight);
            if (!(firstEntry instanceof Node)) {
                pathToAdjNode.putAll(getRestOfPathToAdjNode(firstEntry, graphPixels));
            }
        }
        return pathToAdjNode;
    }

    private Map<Pixel, Integer> getRestOfPathToAdjNode(Pixel firstEntry, Pixel[][] graphPixels) {
        Map<Pixel, Integer> nextEntriesToAdjNode = new LinkedHashMap<>();
        Pixel nextEntry;
        Pixel thisEntry = firstEntry;
        do {
            nextEntry = getNextEntryOfPath(thisEntry, graphPixels);
            int weightToNextEntry = getWeightToNextEntry(thisEntry, nextEntry);
            nextEntriesToAdjNode.put(nextEntry, weightToNextEntry);
            thisEntry = nextEntry;
        } while (!(nextEntry instanceof Node));
        return nextEntriesToAdjNode;
    }

    private Pixel getNextEntryOfPath(Pixel thisEntry, Pixel[][] graphPixels) {
        Pixel nextEntry = null;
        for (int[] deltaMatrix : DELTA_MATRIX) {
            nextEntry = getNextEntry(thisEntry, deltaMatrix[0], deltaMatrix[1], graphPixels);
            if (nextEntry != null) {
                break;
            }
        }
        return nextEntry;
    }

    private static int getWeightToNextEntry(Pixel thisEntry, Pixel nextEntry) {
        int weightToNextEntry = 0;
        if (thisEntry != null && nextEntry != null) {
            weightToNextEntry = thisEntry.getY() == nextEntry.getY() ?
                    abs(nextEntry.getX() - thisEntry.getX()) :
                    abs(nextEntry.getY() - thisEntry.getY());
        }
        return weightToNextEntry;
    }

    private Pixel getNextEntry(Pixel thisEntry, int deltaX, int deltaY, Pixel[][] graphPixels) {
        int xLower = 0, yLower = 0, xUpper = graphPixels.length, yUpper = graphPixels[0].length;
        int x = thisEntry.getX(), y = thisEntry.getY(), xi = x + deltaX, yi = y + deltaY;
        for (; xi >= xLower && xi < xUpper && yi >= yLower && yi < yUpper; xi = xi + deltaX, yi = yi + deltaY) {
            if (abs(xi - x) == 2 || abs(y - yi) == 2) {
                visitedPathPixels.remove(thisEntry); // enables loops
            }
            visitedPathPixels.add(graphPixels[xi - deltaX][yi - deltaY]); //add previous pixel to visited set
            // break out of loop when finding a wall or a visitedPathPixel
            if (!graphPixels[xi][yi].isPath() || visitedPathPixels.contains(graphPixels[xi][yi])) {
                break;
            } else if (graphPixels[xi][yi].isCorner() || graphPixels[xi][yi] instanceof Node) {
                return graphPixels[xi][yi];
            }
        }
        return null;
    }

    public List<Node> getResultPath() {
        return List.copyOf(resultPath);
    }

    public void updateResultPath(List<Node> newResultPath) {
        this.resultPath.clear();
        this.resultPath.addAll(newResultPath);
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return String.format(" [Node (X = %d, Y = %d)] ", this.getX(), this.getY());
    }
}
