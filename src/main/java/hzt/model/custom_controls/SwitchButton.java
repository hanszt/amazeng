package hzt.model.custom_controls;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;

public final class SwitchButton extends Button {

    private final String activeLabel;
    private final String inActiveLabel;
    private final MultipleEventsHandler multipleEventsHandler;

    private boolean active;
    private final boolean switchModeEnabled;
    private String activeStyle;

    public SwitchButton(boolean active, String enabledText, String disabledText) {
        this(active, enabledText, disabledText, null);
    }

    private SwitchButton(boolean active, String enabledText, String disabledText, Node node) {
        super(active ? enabledText : disabledText, node);
        this.active = active;
        this.activeLabel = enabledText;
        this.inActiveLabel = disabledText;
        this.multipleEventsHandler = new MultipleEventsHandler();
        this.multipleEventsHandler.addEventHandler(changeStateAndTextOnClick());
        this.setOnAction(multipleEventsHandler);
        this.switchModeEnabled = true;
    }

    private EventHandler<ActionEvent> changeStateAndTextOnClick() {
        return e -> {
            if (switchModeEnabled) {
                SwitchButton.this.active = !active;
                SwitchButton.this.setButtonLabel();
                SwitchButton.this.setStyleByStatus();
            }
        };
    }

    private void setButtonLabel() {
        super.setText(active ? activeLabel : inActiveLabel);
    }

    private void setStyleByStatus() {
        super.setStyle(activeStyle);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
        setToSwitchModeLabelAndStyle();
    }

    public void setToSwitchModeLabelAndStyle() {
        setButtonLabel();
        setStyleByStatus();
    }

    public void setActiveStyle(String activeStyle) {
        this.activeStyle = activeStyle;
    }

    public MultipleEventsHandler getMultipleEventsHandler() {
        return multipleEventsHandler;
    }
}
