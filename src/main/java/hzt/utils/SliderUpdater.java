package hzt.utils;

@FunctionalInterface
public interface SliderUpdater {

    void updateSliders(double var1, double var2, double var3);
}
