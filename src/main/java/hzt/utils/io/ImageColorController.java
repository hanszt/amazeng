package hzt.utils.io;

import hzt.model.graph.Node;
import hzt.model.maze.Maze;
import hzt.model.maze.Pixel;

import java.awt.Color;
import java.util.List;

public final class ImageColorController {

    private static final Color LIGHT_GRAY = new Color(160, 180, 200);
    private static final Color BROWN = new Color(120, 60, 0);
    private static final Color SAND_COLOR = new Color(250, 235, 200);
    private static final Color TURQUOISE = new Color(50, 200, 200);
    private static final Color WHITE = new Color(255, 255, 255);
    private static final Color GREEN = new Color(0, 255, 0);
    private static final Color FOREST_GREEN = new Color(0, 120, 60);
    private static final Color RED = new Color(255, 0, 0);

    private ImageColorController() {
    }

    static int getRGBForEveryPixel(Maze maze, List<Node> nodesToGoal, int x, int y) {
        final Pixel[][] pixels = maze.getPixels();
        final var currentStartNode = GREEN;
        final var currentEndNode = RED;
        int cost = maze.getGraph() == null ? 1 : maze.getGraph().getExitNode().getCost();
        int rgb;
        if (pixels[x][y].isPath()) {
            rgb = getRGBSinus(SAND_COLOR, LIGHT_GRAY, y, pixels[0].length);
        } else {
            rgb = getRGBSinus(FOREST_GREEN, BROWN, y, pixels[0].length);
        }
        if (pixels[x][y].isCorner()) {
            rgb = nodesToGoal.isEmpty() ? WHITE.getRGB() : rgb;
        }
        if (pixels[x][y] instanceof Node) {
            rgb = nodesToGoal.isEmpty() ? TURQUOISE.getRGB() : rgb;
        }
        if (pixels[x][y] instanceof Node && y == 0) {
            rgb = currentStartNode.getRGB();
        }
        if (pixels[x][y] instanceof Node && y == pixels[0].length - 1) {
            rgb = currentEndNode.getRGB();
        }
        if (pixels[x][y].getSolutionEntryNr() >= 0) {
            rgb = getRGBSinus(currentStartNode, currentEndNode, pixels[x][y].getSolutionEntryNr(), cost);
        }
        return rgb;
    }

    private static int getRGBSinus(Color color1, Color color2, int currentPos, int length) {
        double cosVal = (-Math.cos(Math.PI * currentPos * 1 / length) + 1) / 2;
        int r = (int) (color1.getRed() + cosVal * (color2.getRed() - color1.getRed()));
        int g = (int) (color1.getGreen() + cosVal * (color2.getGreen() - color1.getGreen()));
        int b = (int) (color1.getBlue() + cosVal * (color2.getBlue() - color1.getBlue()));
        return new Color(r, g, b).getRGB();
    }
}
