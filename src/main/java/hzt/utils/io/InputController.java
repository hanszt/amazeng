package hzt.utils.io;

import hzt.model.maze.Maze;
import hzt.model.maze.Pixel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public final class InputController {

    public static final String INPUT_DIRECTORY = "src/main/resources/input";
    private static final Logger LOGGER = LoggerFactory.getLogger(InputController.class);

    private InputController() {
    }

    public static Maze loadInputMaze(String inputName, File file) {
        String[] splitString = inputName.split("[.]");
        String name = splitString[0];
        LOGGER.info("Loading maze {}...", inputName);
        try {
            //read maze file
            BufferedImage image = ImageIO.read(file);
            final var height = image.getHeight();
            final var width = image.getWidth();
            Pixel[][] pixelDataImage = new Pixel[width][height];
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    int rgb = image.getRGB(x, y);
                    boolean isPath = rgb > Color.GRAY.getRGB();
                    pixelDataImage[x][y] = new Pixel(x, y, isPath);
                }
            }
            LOGGER.atInfo().setMessage(() -> "Data of %s has been loaded.%n".formatted(name)).log();
            return new Maze(name, pixelDataImage);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
