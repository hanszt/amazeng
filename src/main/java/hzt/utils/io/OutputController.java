package hzt.utils.io;

import hzt.model.data.AsciiArt;
import hzt.model.graph.Node;
import hzt.model.maze.Maze;
import hzt.model.maze.Pixel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;

public final class OutputController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(OutputController.class);
    private static final String OUTPUT_DIRECTORY = "src/main/resources/output/";

    private OutputController() {
    }

    public static String getOutputImageMessage(List<Maze> mazeList)  {
        return mazeList.isEmpty() ? 
                "No mazes created yet.." :
                (mazeList.size() + " images created. Look in the output folder...");
    }

    public static void createOutputImagesOfMazeList(List<Maze> mazeList, List<Node> nodesToGoal) {
        LOGGER.info(getOutputImageMessage(mazeList));
        for (Maze maze : mazeList) {
            createOutputImage(maze, nodesToGoal);
        }
    }

    private static void createOutputImage(Maze maze, List<Node> nodesToGoal) {
        LOGGER.info("OutputImage is being created...");
        constructMazeImage(maze, nodesToGoal, maze.getId() + maze.getName());
        LOGGER.info("OutputImage of {} created.", maze.getName());
    }

    private static void constructMazeImage(Maze maze, List<Node> nodesToGoal, String outputName) {
        try {
            BufferedImage image = new BufferedImage(maze.getWidth(), maze.getHeight(), BufferedImage.TYPE_INT_RGB);
            Pixel[][] pixels = maze.getPixels();
            for (int y = 0; y < pixels[0].length; y++) {
                for (int x = 0; x < pixels.length; x++) {
                    image.setRGB(x, y, ImageColorController.getRGBForEveryPixel(maze, nodesToGoal, x, y));
                }
            }
            File outputFile = new File(OUTPUT_DIRECTORY + outputName + "." + "png");
            ImageIO.write(image, "png", outputFile);
        } catch (IOException e) {
            LOGGER.error("Could not construct output image", e);
        }
    }

    public static String deleteOutputFiles() {
        File[] files = Optional.ofNullable(new File("src/main/resources/output").listFiles()).orElse(new File[0]);
        int counter = 0;
        for (File file : files) {
            if (isDeleteIfExists(file)) {
                LOGGER.info("{} is deleted", file);
                counter++;
            } else {
                LOGGER.warn("Failed to delete {}", file);
            }
        }
        LOGGER.info("{}{} files deleted.{}", AsciiArt.ANSI_BRIGHT_RED, counter, AsciiArt.ANSI_RESET);
        return counter + " files deleted.";
    }

    private static boolean isDeleteIfExists(File file) {
        try {
            return Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            LOGGER.error("Could not delete image", e);
            return false;
        }
    }
}
