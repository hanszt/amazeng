package hzt.utils;

@FunctionalInterface
public interface ButtonUpdater {

    void update(boolean condition);
}
