package hzt.utils;

import java.util.Arrays;
import java.util.function.IntFunction;

public final class Grid2DUtils {

    private Grid2DUtils() {
    }

    public static <T> T[][] copyGrid(T[][] grid, IntFunction<T[][]> generator) {
        return Arrays.stream(grid)
                .map(row -> Arrays.copyOf(row, row.length))
                .toArray(generator);
    }
}
