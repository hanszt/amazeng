package hzt.view;

import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.random;
import static javafx.scene.paint.Color.BLACK;
import static javafx.scene.paint.Color.DARKBLUE;
import static javafx.scene.paint.Color.DARKCYAN;
import static javafx.scene.paint.Color.DARKGRAY;
import static javafx.scene.paint.Color.DARKGREEN;
import static javafx.scene.paint.Color.DARKMAGENTA;
import static javafx.scene.paint.Color.DARKORCHID;
import static javafx.scene.paint.Color.DARKRED;
import static javafx.scene.paint.Color.LIGHTBLUE;
import static javafx.scene.paint.Color.LIGHTCYAN;
import static javafx.scene.paint.Color.LIGHTGRAY;
import static javafx.scene.paint.Color.LIGHTGREEN;
import static javafx.scene.paint.Color.LIGHTSEAGREEN;
import static javafx.scene.paint.Color.LIGHTSKYBLUE;
import static javafx.scene.paint.Color.LIGHTSLATEGRAY;
import static javafx.scene.paint.Color.LIGHTYELLOW;
import static javafx.scene.paint.Color.NAVY;
import static javafx.scene.paint.Color.SADDLEBROWN;
import static javafx.scene.paint.Color.SANDYBROWN;
import static javafx.scene.paint.Color.WHITE;
import static javafx.scene.paint.Color.YELLOW;
import static javafx.scene.paint.Color.rgb;

public class ColorController {

    private static final List<Color> LIGHT_COLOR_LIST = new ArrayList<>();
    private static final List<Color> DARK_COLOR_LIST = new ArrayList<>();

    static {
        DARK_COLOR_LIST.add(NAVY);
        DARK_COLOR_LIST.add(SADDLEBROWN);
        DARK_COLOR_LIST.add(DARKBLUE);
        DARK_COLOR_LIST.add(DARKGREEN);
        DARK_COLOR_LIST.add(DARKCYAN);
        DARK_COLOR_LIST.add(DARKMAGENTA);
        DARK_COLOR_LIST.add(DARKRED);
        DARK_COLOR_LIST.add(BLACK);
        DARK_COLOR_LIST.add(DARKGRAY);
        DARK_COLOR_LIST.add(DARKORCHID);
        DARK_COLOR_LIST.add(DARKGRAY);
        LIGHT_COLOR_LIST.add(LIGHTBLUE);
        LIGHT_COLOR_LIST.add(SANDYBROWN);
        LIGHT_COLOR_LIST.add(LIGHTGREEN);
        LIGHT_COLOR_LIST.add(WHITE);
        LIGHT_COLOR_LIST.add(LIGHTGRAY);
        LIGHT_COLOR_LIST.add(LIGHTCYAN);
        LIGHT_COLOR_LIST.add(LIGHTSEAGREEN);
        LIGHT_COLOR_LIST.add(YELLOW);
        LIGHT_COLOR_LIST.add(LIGHTSKYBLUE);
        LIGHT_COLOR_LIST.add(LIGHTSLATEGRAY);
        LIGHT_COLOR_LIST.add(LIGHTYELLOW);
    }

    Color pathColor;
    Color bottomPathColor;
    Color wallColor;
    Color bottomWallColor;
    Color nodeColor;
    Color beginSolutionColor;
    Color endSolutionColor;
    Color backGroundColor;

    public ColorController() {
        this.wallColor = getRandomColor(DARK_COLOR_LIST);
        this.bottomWallColor = getRandomColor(DARK_COLOR_LIST);
        this.pathColor = getRandomColor(LIGHT_COLOR_LIST);
        this.bottomPathColor = getRandomColor(LIGHT_COLOR_LIST);
        this.nodeColor = DARKBLUE;
        this.backGroundColor = BLACK;
        this.beginSolutionColor = rgb(0, 255, 0);
        this.endSolutionColor = rgb(255, 0, 0);
    }

    public Color getColorByHeight(Color color1, Color color2, int currentPos, int length) {
        final double sine = (-Math.cos(Math.PI * currentPos / length) + 1) / 2;
        double r = (color1.getRed() + sine * (color2.getRed() - color1.getRed()));
        double g = (color1.getGreen() + sine * (color2.getGreen() - color1.getGreen()));
        double b = (color1.getBlue() + sine * (color2.getBlue() - color1.getBlue()));
        return new Color(r, g, b, 1);
    }

    Runnable colorUpdater;

    void resetColors() {
        wallColor = getRandomColor(DARK_COLOR_LIST);
        bottomWallColor = getRandomColor(DARK_COLOR_LIST);
        pathColor = getRandomColor(LIGHT_COLOR_LIST);
        bottomPathColor = getRandomColor(LIGHT_COLOR_LIST);
        nodeColor = DARKBLUE;
        backGroundColor = BLACK;
        beginSolutionColor = rgb(0, 255, 0);
        endSolutionColor = rgb(255, 0, 0);
        colorUpdater.run();
    }

    private Color getRandomColor(List<Color> colorList) {
        return colorList.get((int) (random() * colorList.size()));
    }

    public Color getBackGroundColor() {
        return backGroundColor;
    }

    public Color getPathColor() {
        return pathColor;
    }

    public Color getBPathColor() {
        return bottomPathColor;
    }
}
