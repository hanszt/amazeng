package hzt.view;

import hzt.controller.AppConstants;
import hzt.controller.AppManager;
import hzt.controller.mazecontroller.MazeGenerator;
import hzt.model.data.AsciiArt;
import hzt.model.data.MazeCreatorEnum;
import hzt.model.maze.Maze;
import hzt.model.maze.Pixel;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Random;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public final class AnimateGen extends GenVars {

    private static final double EXTRA_WALL_KNOCKDOWN_RATIO = 4. / 9;

    private final Random random = new Random();
    private final MazeGenerator mg = new MazeGenerator(random);
    private MazeCreatorEnum mazeGenPicker = MazeCreatorEnum.RANDOM_NEIGHBOUR_PICKER;
    private Deque<Pixel> cellStack; //A cell is a path pixel at odd x,y positions with 4 pixels around it
    private Pixel thisCell;

    private AnimateSolve as;

    public AnimateGen(AppManager m, ColorController c) {
       super(m, c);
    }

    public void initializeGenVars(AnimateSolve as, Canvas canvas) {
        this.as = as;
        genSolveCounter++;
        resetGenVars();
        mazePixels = MazeGenerator.createGridWithMazeCells(mazeSize.width, mazeSize.height);
        totalCells = ((mazeSize.width - 1) * (mazeSize.height - 1)) / 4;
        mazeUnitSize = canvas.getHeight() / mazeSize.height;
        int startX = mg.getRandCellPos(mazeSize.width);
        int startY = mg.getRandCellPos(mazeSize.height);
        thisCell = mazePixels[startX][startY];
    }

    public void manageMazeGeneration(StatsController s, Canvas mazeCanvas) {
        frameCount++;
        GraphicsContext gc = mazeCanvas.getGraphicsContext2D();
        if (Double.compare(startTimeGen, 0.0) == 0) {
            initiateGen(mazeCanvas);
        }
        double extraKnockdownThreshold = mazePixels.length *
                mazePixels[0].length * EXTRA_WALL_KNOCKDOWN_RATIO *
                (imperfectPercentage / 100);
        if (visited < totalCells) {
            generatePerfectMaze(gc);
        } else if (visited == totalCells && extraKnockdown < extraKnockdownThreshold) {
            makeMazeImperfect(gc, extraKnockdownThreshold);
        } else {
            finishMazeGeneration(gc);
        }
        s.getGenStats();
        if (followDrawSpotButton.isActive()) {
            m.followDrawSpot(thisCell);
        }
    }

    private void initiateGen(Canvas canvas) {
        startTimeGen = System.nanoTime();
        GraphicsContext gc = canvas.getGraphicsContext2D();
        System.out.println("Animation of maze generation starting...");
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        cellStack = new ArrayDeque<>();
        System.out.println("Animation started...");
    }

    double getGenTime() {
        return ((stopTimeGen != 0 ? stopTimeGen : System.nanoTime()) - startTimeGen) / 1e9;
    }

    public void drawMazeWalls(Canvas bgCanvas, double angle) {
        double v0 = sin(angle), v1 = cos(angle);
//        out.printf("angle: %.2f, v0 = %.2f, v1 = %.2f, v2 = %.2f, v3 = %.2f\n", angle, v0, v1, v2, v3);
        final var lg = new LinearGradient(v0, v1, 0, 0, true, CycleMethod.REFLECT,
                new Stop(0, c.wallColor), new Stop(1, c.bottomWallColor));
        GraphicsContext gc = bgCanvas.getGraphicsContext2D();
        gc.setFill(lg);
        gc.fillRect(0, 0, bgCanvas.getWidth(), bgCanvas.getHeight());
    }

    private void generatePerfectMaze(GraphicsContext graphics) {
        graphics.setFill(c.getColorByHeight(c.pathColor, c.bottomPathColor, thisCell.getY(), mazeSize.height));
        List<Pixel> closedAdjCells = MazeGenerator.findClosedAdjCells(mazePixels, thisCell);
        if (!closedAdjCells.isEmpty()) {
            graphics.fillRect(thisCell.getX() * mazeUnitSize, thisCell.getY() * mazeUnitSize, mazeUnitSize, mazeUnitSize);
            nrOfCreatedPaths++;
            Pixel adjCell = getAdjacentCell(closedAdjCells, visited);
            knockDownWall(thisCell, adjCell, graphics);
            cellStack.push(thisCell);
            thisCell = adjCell;
            graphics.fillRect(thisCell.getX() * mazeUnitSize, thisCell.getY() * mazeUnitSize, mazeUnitSize, mazeUnitSize);
            visited++;
        } else {
            thisCell = cellStack.pop();
        }
    }

    private Pixel getAdjacentCell(List<Pixel> closedAdjCells, int visited) {
        return switch (mazeGenPicker) {
            case RANDOM_NEIGHBOUR_PICKER -> mg.getRandAdjCell(closedAdjCells);
            case FIBONACCI_NEIGHBOUR_PICKER -> MazeGenerator.getFibAdjCell(closedAdjCells, visited);
            case MODULO_NEIGHBOUR_PICKER -> MazeGenerator.getModAdjCell(closedAdjCells, visited);
            case VERTICAL_NEIGHBOUR_PICKER -> MazeGenerator.getVerticalAdjCell(closedAdjCells);
            case HORIZONTAL_NEIGHBOUR_PICKER -> MazeGenerator.getHorizontalAdjCell(closedAdjCells);
            case SPIRAL_NEIGHBOUR_PICKER -> MazeGenerator.CustomShapePicker.getAdjCellForSpiralShape(closedAdjCells, visited);
            case TEST_NEIGHBOUR_PICKER -> MazeGenerator.CustomShapePicker.getTestAdjCell(closedAdjCells, visited);
        };
    }

    private void knockDownWall(Pixel thisCell, Pixel adjCell, GraphicsContext path) {
        final int x, y;
        if (thisCell.getX() == adjCell.getX()) {
            x = thisCell.getX();
            y = (thisCell.getY() + adjCell.getY()) / 2;
        } else {
            x = (thisCell.getX() + adjCell.getX()) / 2;
            y = thisCell.getY();
        }
        path.fillRect(x * mazeUnitSize, y * mazeUnitSize, mazeUnitSize, mazeUnitSize);
        mazePixels[x][y].setPath(true);
        nrOfCreatedPaths++;
    }

    private void makeMazeImperfect(GraphicsContext path, double threshold) {
        int newExtraKnockdown = extraKnockdown;
        double startTimePrevWhileLoop = System.nanoTime();
        while (newExtraKnockdown == extraKnockdown && startTimePrevWhileLoop - System.nanoTime() < 1e4 && extraKnockdown < threshold) {
            int x = random.nextInt(mazePixels.length - 1);
            int y = random.nextInt(mazePixels[0].length - 1);
            final var xIsOddAndYisEven = x % 2 == 1 && y % 2 == 0 && y != 0;
            final var xIsEvenAndYIsOdd = x % 2 == 0 && y % 2 == 1 && x != 0;
            final var isWall = !mazePixels[x][y].isPath();
            if ((xIsOddAndYisEven || xIsEvenAndYIsOdd) && isWall) {
                path.setFill(c.getColorByHeight(c.pathColor, c.bottomPathColor, y, mazeSize.height));
                path.fillRect(x * mazeUnitSize, y * mazeUnitSize, mazeUnitSize, mazeUnitSize);
                mazePixels[x][y].setPath(true);
                extraKnockdown++;
                startTimePrevWhileLoop = System.nanoTime();
                nrOfCreatedPaths++;
            }
        }
    }

    private void finishMazeGeneration(GraphicsContext graphics) {
        m.stopSim();
        entranceX = /*1;*/ mg.getRandCellPos(mazeSize.width);
        exitX = /* mazeSize.width - 2;*/ mg.getRandCellPos(mazeSize.width);
        graphics.setFill(c.beginSolutionColor);
        graphics.fillRect(entranceX * mazeUnitSize, 0, mazeUnitSize, mazeUnitSize);
        graphics.setFill(c.endSolutionColor);
        graphics.fillRect(exitX * mazeUnitSize, (mazePixels[0].length - 1) * mazeUnitSize, mazeUnitSize, mazeUnitSize);
        nrOfCreatedPaths = nrOfCreatedPaths + 2;
        mazePixels[entranceX][0].setPath(true);
        mazePixels[exitX][mazePixels[0].length - 1].setPath(true);
        stopTimeGen = System.nanoTime();
        addMazeToMazeList(new Maze("simulatedMaze", mazePixels));
        System.out.printf("Animation of generation finished in %.3f seconds.%n%s%n",
                (stopTimeGen - startTimeGen) / 1e9, AppConstants.DOTTED_LINE);
        as.setupSolve();
        as.getPauseButtonUpdater().update(true);
        System.out.println(AsciiArt.ANSI_GREEN + "Press play to continue maze solve..." + AsciiArt.ANSI_RESET);
    }

    private void resetGenVars() {
        extraKnockdown = 0;
        startTimeGen = stopTimeGen = 0;
        visited = nrOfCreatedPaths = 1;
    }

    MazeCreatorEnum getMazeGenPicker() {
        return mazeGenPicker;
    }

    void setMazeGenPicker(MazeCreatorEnum mazeGenPicker) {
        this.mazeGenPicker = mazeGenPicker;
    }

}

