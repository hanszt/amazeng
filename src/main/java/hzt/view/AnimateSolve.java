package hzt.view;

import hzt.controller.AppConstants;
import hzt.controller.AppManager;
import hzt.model.data.AsciiArt;
import hzt.model.graph.Edge;
import hzt.model.graph.Node;
import hzt.model.maze.Pixel;
import hzt.utils.ButtonUpdater;
import hzt.utils.Grid2DUtils;
import hzt.utils.SliderUpdater;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class AnimateSolve extends SolveController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnimateSolve.class);

    private SliderUpdater speedSliderUpdater;
    private Runnable modeDropdownMenuUpdater;
    private ButtonUpdater pauseButtonUpdater;
    private ButtonUpdater restartButtonUpdater;
    private int pathDrawCounter = 0;

    public AnimateSolve(AppManager m, AnimateGen ag, ColorController c) {
        super(m, ag, c);
    }

    private void setInitialSolveConditions() {
        visited = new HashSet<>();
        unsettledNodes = new HashSet<>();
        stack = new ArrayDeque<>();
        queue = new LinkedList<>();
        source.setCost(0);
        unsettledNodes.add(source); // for dijkstra
        stack.push(source); // for depthFirstSearch
        if (solveSelector.equals(AppConstants.BREADTH_FIRST_SEARCH)) {
            queue.add(source); // for breadthFirstSearch
            visited.add(source); // for breadthFirstSearch
        }
    }

    private void resetSolveVars() {
        //is needed because classes are called recursively
        m.settStop(0);
        startTimeSolve = 0;
        m.setStopTimeSim(0);
        solutionEntryNumber = pathDrawCounter = 0;
        solutionDrawing = false;
    }

    void setupSolve() {
        resetSolveVars();
        LOGGER.info("Animation of maze solve starting...");
        source = ag.getCurrentMaze().getGraph().getStartNode();
        goal = ag.getCurrentMaze().getGraph().getExitNode();
        setInitialSolveConditions();
        m.initiateSolveSim();
    }

    void plotUnit(GraphicsContext graphics, Pixel pixel) {
        graphics.fillRect(pixel.getX() * ag.mazeUnitSize, pixel.getY() * ag.mazeUnitSize, ag.mazeUnitSize, ag.mazeUnitSize);
    }

    private void setupResultPathDraw(GraphicsContext graphics) {
        speedSliderUpdater.updateSliders(ag.mazeSize.height / 4., ag.mazeSize.height, 0);
        nodesToGoal.clear();
        nodesToGoal.addAll(goal.getResultPath());
        nodesToGoal.add(goal);
        solutionLength = goal.getCost();
        LOGGER.info("Solution Found With {}!", solveSelector);
        LOGGER.atInfo()
                .setMessage(() -> "Solve finished in %.3f seconds.".formatted((stopTimeSolve - startTimeSolve) / 1e9))
                .log();
        LOGGER.info("framerate adjusted. Drawing solution path...");
        LOGGER.info(AppConstants.DOTTED_LINE);
        ag.mazePixels = Grid2DUtils.copyGrid(ag.getCurrentMaze().getPixels(), Pixel[][]::new);
        solutionEntryList.clear();
        solutionEntryList.addAll(pathPixelsFromStartToFinish(nodesToGoal));
        int xExit = ag.getCurrentMaze().getGraph().getExitNode().getX();
        int iForLastEntry = solutionEntryList.size() - 2;
        ag.mazePixels[xExit][ag.mazePixels[0].length - 1].setSolutionEntryNr(solutionEntryList.get(iForLastEntry).getSolutionEntryNr());
        graphics.setFill(c.endSolutionColor);
        plotUnit(graphics, ag.mazePixels[xExit][ag.mazePixels[0].length - 1]);
    }

    private static List<Pixel> pathPixelsFromStartToFinish(List<Node> nodesToGoal) {
        List<Pixel> solutionPath = new LinkedList<>();
        solutionPath.add(nodesToGoal.get(0));
        for (int i = 0; i < nodesToGoal.size() - 1; i++) {
            final var edges = nodesToGoal.get(i).getEdges();
            solutionPath.addAll(pixelsToNeighbor(edges, nodesToGoal.get(i + 1)));
        }
        return solutionPath;
    }

    private static List<Pixel> pixelsToNeighbor(List<Edge> edges, Node neighbor) {
        return edges.stream()
                .filter(edge -> edge.getAdjNode() == neighbor)
                .findFirst()
                .map(Edge::getEntriesToAdjNode)
                .orElse(Collections.emptyList());
    }

    void plotResultPath(GraphicsContext graphics) {
        if (!solutionDrawing) {
            solutionDrawing = true;
            setupResultPathDraw(graphics);
        }
        if (solutionEntryNumber < solutionEntryList.size() - 1) {
            setSolutionPath(ag.getCurrentMaze().getPixels(), solutionEntryNumber++, graphics);
        } else {
            stopSolveSimAndRestart(graphics);
        }
    }

    private void setSolutionPath(Pixel[][] pixels, int i, GraphicsContext graphics) {
        Pixel currentPixel = solutionEntryList.get(i);
        if (ag.followDrawSpotButton.isActive()) {
            m.followDrawSpot(currentPixel);
        }
        Pixel nextPixel = solutionEntryList.get(i + 1);
        int xCurrent = currentPixel.getX();
        int yCurrent = currentPixel.getY();
        int xNext = nextPixel.getX();
        int yNext = nextPixel.getY();
        if (yCurrent == yNext) {
            setSolutionForHorPaths(pixels, xCurrent, xNext, yCurrent, graphics);
        } else if (xCurrent == xNext) {
            setSolutionForVerPaths(pixels, xCurrent, yCurrent, yNext, graphics);
        } else {
            throw new IllegalStateException("Unexpected state...");
        }
    }

    private void setSolutionForHorPaths(Pixel[][] pixels, int xCurrent, int xNext, int yCurrent, GraphicsContext graphics) {
        for (int x = 0; x < Math.abs(xCurrent - xNext); x++) {
            pathDrawCounter++;
            graphics.setFill(c.getColorByHeight(c.beginSolutionColor, c.endSolutionColor, pathDrawCounter, solutionLength));
            if (xCurrent < xNext) {
                pixels[xCurrent + x][yCurrent].setSolutionEntryNr(pathDrawCounter);
                plotUnit(graphics, pixels[xCurrent + x][yCurrent]);
            } else {
                pixels[xCurrent - x][yCurrent].setSolutionEntryNr(pathDrawCounter);
                plotUnit(graphics, pixels[xCurrent - x][yCurrent]);
            }
        }
    }

    private void setSolutionForVerPaths(Pixel[][] pixels, int xCurrent, int yCurrent, int yNext, GraphicsContext graphics) {
        for (int y = 0; y < Math.abs(yCurrent - yNext); y++) {
            pathDrawCounter++;
            graphics.setFill(c.getColorByHeight(c.beginSolutionColor, c.endSolutionColor, pathDrawCounter, solutionLength));
            if (yCurrent < yNext) {
                pixels[xCurrent][yCurrent + y].setSolutionEntryNr(pathDrawCounter);
                plotUnit(graphics, pixels[xCurrent][yCurrent + y]);
            } else {
                pixels[xCurrent][yCurrent - y].setSolutionEntryNr(pathDrawCounter);
                plotUnit(graphics, pixels[xCurrent][yCurrent - y]);
            }
        }
    }

    private void stopSolveSimAndRestart(GraphicsContext graphics) {
        if (Double.compare(m.gettStop(), 0.0) == 0) {
            m.settStop(System.nanoTime());
            graphics.setStroke(Color.ORANGE);
            m.setStopTimeSim(System.nanoTime());
            LOGGER.info("Animation finished");
            LOGGER.info(AppConstants.DOTTED_LINE);
            restartButtonUpdater.update(true);
            LOGGER.info("{}Press play to continue next maze generation...{}", AsciiArt.ANSI_GREEN, AsciiArt.ANSI_RESET);
        }
    }

    public SliderUpdater getSpeedSliderUpdater() {
        return speedSliderUpdater;
    }

    public void setSpeedSliderUpdater(SliderUpdater speedSliderUpdater) {
        this.speedSliderUpdater = speedSliderUpdater;
    }

    public Runnable getModeDropdownMenuUpdater() {
        return modeDropdownMenuUpdater;
    }

    public void setModeDropdownMenuUpdater(Runnable modeDropdownMenuUpdater) {
        this.modeDropdownMenuUpdater = modeDropdownMenuUpdater;
    }

    public ButtonUpdater getPauseButtonUpdater() {
        return pauseButtonUpdater;
    }

    public void setPauseButtonUpdater(ButtonUpdater pauseButtonUpdater) {
        this.pauseButtonUpdater = pauseButtonUpdater;
    }

    public ButtonUpdater getRestartButtonUpdater() {
        return restartButtonUpdater;
    }

    public void setRestartButtonUpdater(ButtonUpdater restartButtonUpdater) {
        this.restartButtonUpdater = restartButtonUpdater;
    }
}
