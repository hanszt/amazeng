package hzt.view;

import hzt.controller.AppConstants;
import hzt.controller.AppManager;
import hzt.model.graph.Edge;
import hzt.model.graph.Node;
import hzt.model.maze.Pixel;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public abstract class SolveController extends SolveVars {

    String solveSelector = AppConstants.DIJKSTRA;

    Set<Node> visited;
    Set<Node> unsettledNodes;
    Queue<Node> queue;
    Deque<Node> stack;

    SolveController(AppManager m, AnimateGen ag, ColorController c) {
        super(m, ag, c);
    }

    public void manageMazeSolve(StatsController s, Canvas searchCanvas, Canvas solutionCanvas) {
        m.setFrameCount(m.getFrameCount() + 1);
        if (Double.compare(startTimeSolve, 0.0) == 0) {
            startTimeSolve = System.nanoTime();
            System.out.println("Search started...");
        }
        if (!visited.contains(goal)) {
            useSelectedSolveMethod(searchCanvas.getGraphicsContext2D());
            stopTimeSolve = System.nanoTime();
        } else {
            plotResultPath(solutionCanvas.getGraphicsContext2D());
        }
        s.getSolveStats();
    }

    abstract void plotResultPath(GraphicsContext gc);

    private void useSelectedSolveMethod(GraphicsContext graphics) {
        Node currentNode;
        graphics.setFill(c.nodeColor);
        if (solveSelector.equals(AppConstants.BREADTH_FIRST_SEARCH)) {
            currentNode = useBreadthFirstTraversal(graphics);
        } else if (solveSelector.equals(AppConstants.DEPTH_FIRST_SEARCH)) {
            currentNode = useDepthFirstSearch(graphics);
        } else {
            currentNode = searchWithDijkstra(graphics);
        }
        if (ag.followDrawSpotButton.isActive()) {
            m.followDrawSpot(currentNode);
        }
    }

    private Node searchWithDijkstra(GraphicsContext graphics) {
        Node currentNode = getLowestDistanceNode(unsettledNodes), adjacentNode;
        int edgeWeight;
        solutionLength = currentNode.getCost();
        if (currentNode.getY() != 0) {
            plotUnit(graphics, currentNode);
        }
        unsettledNodes.remove(currentNode);
        for (Map.Entry<Node, Edge> entry : currentNode.getAdjacentNodes().entrySet()) {
            adjacentNode = entry.getKey();
            edgeWeight = entry.getValue().getWeight();
            if (!visited.contains(adjacentNode)) {
                calculateMinimumDistance(currentNode, adjacentNode, edgeWeight);
                unsettledNodes.add(adjacentNode);
            }
        }
        visited.add(currentNode);
        return currentNode;
    }

    abstract void plotUnit(GraphicsContext graphics, Pixel current);

    private Node getLowestDistanceNode(Set<Node> unsettledNodes) {
        Node lowestDistanceNode = null;
        int lowestDistance = Integer.MAX_VALUE;
        for (Node node : unsettledNodes) {
            int distance = node.getCost();
            if (distance < lowestDistance) {
                lowestDistance = distance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }

    private void calculateMinimumDistance(Node currentNode, Node adjNode, int edgeWeight) {
        int sourceDistance = currentNode.getCost();
        if (sourceDistance + edgeWeight < adjNode.getCost()) {
            adjNode.setCost(sourceDistance + edgeWeight);
            List<Node> shortestPath = new LinkedList<>(currentNode.getResultPath());
            shortestPath.add(currentNode);
            adjNode.updateResultPath(shortestPath);
        }
    }

    private Node useDepthFirstSearch(GraphicsContext graphics) {
        Node currentNode = null;
        if (!stack.isEmpty()) {
            currentNode = stack.pop();
            solutionLength = currentNode.getCost();
            if (currentNode.getY() != 0) {
                plotUnit(graphics, currentNode);
            }
            if (!visited.contains(currentNode)) {
                visited.add(currentNode);
                for (Map.Entry<Node, Edge> entry : currentNode.getAdjacentNodes().entrySet()) {
                    Node adjNode = entry.getKey();
                    updateAdjNodeDepthFirst(currentNode, adjNode, entry.getValue());
                    stack.push(adjNode);
                }
            }
        }
        return currentNode;
    }

    private void updateAdjNodeDepthFirst(Node currentNode, Node adjNode, Edge value) {
        adjNode.setCost(currentNode.getCost() + value.getWeight());
        List<Node> resultPath = new LinkedList<>(currentNode.getResultPath());
        resultPath.add(currentNode);
        adjNode.updateResultPath(resultPath);
    }

    private Node useBreadthFirstTraversal(GraphicsContext graphics) {
        Node currentNode = null;
        if (!queue.isEmpty()) {
            currentNode = queue.poll(); // look en remove from queue
            solutionLength = currentNode.getCost();
            if (currentNode.getY() != 0) {
                plotUnit(graphics, currentNode);
            }
            for (Map.Entry<Node, Edge> entry : currentNode.getAdjacentNodes().entrySet()) {
                Node adjNode = entry.getKey();
                if (!visited.contains(adjNode)) {
                    updateAdjNodeBreadthFirst(currentNode, adjNode, entry.getValue());
                    visited.add(adjNode);
                    queue.add(adjNode);
                }
            }
        }
        return currentNode;
    }

    private void updateAdjNodeBreadthFirst(Node currentNode, Node adjNode, Edge value) {
        adjNode.setCost(currentNode.getCost() + value.getWeight());
        List<Node> resultPath = new LinkedList<>(currentNode.getResultPath());
        resultPath.add(currentNode);
        adjNode.updateResultPath(resultPath);
    }

    String getSolveSelector() {
        return solveSelector;
    }

    void setSolveSelector(String solveSelector) {
        this.solveSelector = solveSelector;
    }
}
