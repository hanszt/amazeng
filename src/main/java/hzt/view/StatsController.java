package hzt.view;

import hzt.controller.AppManager;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import static hzt.controller.AppConstants.INIT_FRAMERATE;
import static hzt.controller.AppConstants.MENU_WIDTH;
import static javafx.scene.paint.Color.*;

public class StatsController {

    private static final int STAT_FONT_SIZE = 10;
    private static final int NUMBER_POS = 170;

    private final AppManager m;
    private final AnimateGen ag;
    private final AnimateSolve as;

    private GraphicsContext graphics;
    private boolean initStats = true; // this is needed to set the initial conditions

    public StatsController(AppManager m, AnimateGen ag, AnimateSolve as) {
        this.m = m;
        this.ag = ag;
        this.as = as;
    }

    Canvas setupStatsCanvas(@SuppressWarnings("SameParameterValue") int sideLength) {
        Canvas statsCanvas = new Canvas(sideLength, sideLength);
        graphics = statsCanvas.getGraphicsContext2D();
        return statsCanvas;
    }

    void getGenStats() {
        if (initStats) {
            initStats = false;
            graphics.setTextAlign(TextAlignment.LEFT);
            graphics.setFont(Font.font("", STAT_FONT_SIZE));
            graphics.setFill(BLACK);
            graphics.fillRect(0, 0, MENU_WIDTH, m.getScene().getHeight());
        }
        fillStatsWithBlackBox(graphics);
        graphics.setFill(YELLOW);
        getStatNamesGen(graphics);
        graphics.fillText(String.format("\n %d\n %d\n %d\n%.2f\n%.3f s\n%.0f f/s\n",
                ag.mazeSize.width, ag.mazeSize.height, ag.nrOfCreatedPaths, ag.imperfectPercentage,
                ag.getGenTime(), (int) m.getTimeline().getRate() * INIT_FRAMERATE), NUMBER_POS, STAT_FONT_SIZE);
    }

    private void getStatNamesGen(GraphicsContext graphics) {
        graphics.fillText("Maze generation stats:\nMaze width: \nMaze height: \n" +
                "PathUnits created: \nImperfectness percentage: \nGeneration initStats:\nFramerate:\n", 5, STAT_FONT_SIZE);
    }

    private void fillStatsWithBlackBox(GraphicsContext graphics) {
        graphics.setFill(BLACK);
        graphics.fillRect(0, 0, MENU_WIDTH, m.getScene().getHeight() / 4);
    }

    private  final int SOLVE_STATS_POS = 10;

    private  void getStatNamesSolve(GraphicsContext graphics) {
        graphics.setTextAlign(TextAlignment.LEFT);
        graphics.setFont(Font.font("", STAT_FONT_SIZE));
        graphics.setFill(LIGHTGREEN);
        graphics.fillText("Maze solve stats:\nVisited nodes:\nSolution length:\nSolve initStats:\nPathDrawTime:", 5, STAT_FONT_SIZE * SOLVE_STATS_POS);
        graphics.setFill((ORANGE));
        graphics.fillText("\n\n\n\n\n\nRuntime:", 5, STAT_FONT_SIZE * SOLVE_STATS_POS);
    }

     void getSolveStats() {
        getGenStats();
        getStatNamesSolve(graphics);
        graphics.setFill(LIGHTGREEN);
        graphics.fillText(String.format("\n%d\n%d\n%.3f s\n%.3f s",
                ag.visited, as.solutionLength, as.getSolveTime(), getPathDrawTime()), NUMBER_POS, STAT_FONT_SIZE * SOLVE_STATS_POS);
        getFinalStats(graphics);
    }

    private double getPathDrawTime() {
        return as.stopTimeSolve != 0 ? ag.getGenTime() : 0;
    }

    private  void getFinalStats(GraphicsContext graphics) {
        graphics.setFill(ORANGE);
        graphics.fillText(String.format("\n\n\n\n\n\n%.3f s", as.getSolveTime()), NUMBER_POS, STAT_FONT_SIZE * SOLVE_STATS_POS);
    }

    public void setInitStats(boolean initStats) {
        this.initStats = initStats;
    }
}
