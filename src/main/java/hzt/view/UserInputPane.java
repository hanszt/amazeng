package hzt.view;

import hzt.controller.AppConstants;
import hzt.controller.AppManager;
import hzt.controller.mazecontroller.MazeGenerator;
import hzt.model.data.AsciiArt;
import hzt.utils.ButtonUpdater;
import hzt.utils.SliderUpdater;
import hzt.utils.io.InputController;
import hzt.utils.io.OutputController;
import hzt.model.custom_controls.SwitchButton;
import hzt.model.data.MazeCreatorEnum;
import hzt.model.maze.Maze;
import javafx.animation.Animation;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Dimension;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;

public class UserInputPane extends GridPane {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserInputPane.class);

    private static final int DIST_FROM_EDGE = 20;
    private static final int PREF_WIDTH_CONTROLS = 200;
    private static final double OPACITY_VALUE = 0.7;

    private static final String MAZE_WIDTH_LABEL = "maze width: ";
    private static final String MAZE_HEIGHT_LABEL = "Maze height: ";
    private static final String GENERATION_MODE = "Generation mode";


    private static final int MAX_NUMBER_OF_INSTANCES = 4;
    private ButtonUpdater rotateSliderDisabler;

    private final AppManager m;
    private final AnimateGen ag;
    private final AnimateSolve as;
    private final ColorController c;
    private final Canvas statsCanvas;

    private int vPosControls = 0;

    private ColorPicker wallColorPicker;
    private ColorPicker bWallColorPicker;
    private ColorPicker pathColorPicker;
    private ColorPicker bPathColorPicker;
    private ColorPicker beginSolveCp;
    private ColorPicker endSolveCp;
    private ColorPicker nodeColorPicker;
    private ColorPicker backGroundColorPicker;
    private SliderUpdater sliderUpdater;

    private Slider bcBrightnessSlider;


    private final ComboBox<MazeDimension> mazeWidthMenu = new ComboBox<>();
    private final ComboBox<MazeDimension> mazeHeightMenu = new ComboBox<>();

    public UserInputPane(AppManager m, AnimateGen ag, AnimateSolve sc, ColorController c) {
        this.m = m;
        this.ag = ag;
        this.as = sc;
        this.c = c;
        configure();
        setupControls(m.getBgCanvas());
        statsCanvas = m.getS().setupStatsCanvas(PREF_WIDTH_CONTROLS);
        this.add(statsCanvas, 0, 0, 2, 1);
        statsCanvas.setVisible(false);
    }

    @Override
    public final void add(Node child, int columnIndex, int rowIndex, int colSpan, int rowSpan) {
        super.add(child, columnIndex, rowIndex, colSpan, rowSpan);
    }

    private void configure() {
        this.setHgap(2);
        this.setVgap(4);
        this.setAlignment(Pos.CENTER);
        double yScaleFactor = m.getScene().getHeight() / AppConstants.INIT_SCENE_SIZE.height;
        this.setScaleY(Math.max(yScaleFactor, 1));
        this.setPadding(new Insets(DIST_FROM_EDGE, DIST_FROM_EDGE, DIST_FROM_EDGE * 2.0, DIST_FROM_EDGE));
    }

    private void setupControls(Canvas bgCanvas) {
        this.setOpacity(OPACITY_VALUE);
        setupLabels();
        setupDropdownMenus();
        setupButtons();
        setupColorPickers();
        setupSliders(bgCanvas);
        vPosControls = 0;
        for (Node n : this.getChildren()) {
            n.setCursor(Cursor.HAND);
        }
    }

    private void setupLabels() {
        setupLabel(AppConstants.INIT_SCENE_SIZE.height, vPosControls++);
    }

    private void setupLabel(int prefHeight, int vPos) {
        final Label label = new Label();
        label.setTextFill(c.backGroundColor);
        label.setPrefHeight(UserInputPane.PREF_WIDTH_CONTROLS);
        label.setPrefHeight(prefHeight);
        this.add(label, 0, vPos);
    }

    private void setupDropdownMenus() {
        setupLabel("Generation methods");
        setupDropdownMenuInputMaze();
        setupDropdownMenuMazeWidth();
        setupDropdownMenuMazeHeight();
        setupDropdownMenuImperfect();
        setupDropdownMenuGenerationMethods();
        setupLabel("Solve methods");
        setupDropdownMenuSolveMethods();
    }

    private void setupLabel(String text) {
        Label label = new Label(text);
        label.setTextFill(Color.YELLOW);
        this.add(label, 0, vPosControls++);
    }

    private void setupButtons() {
        Button print = setupButton("print to output", 0, vPosControls);
        Button delete = setupButton("delete files in output", 1, vPosControls++);
        Button restart = setupButton("restart", 0, vPosControls);
        SwitchButton pauseButton = setupSwitchButton(false, "play sim", "pause sim", 1, vPosControls++);
        Button resetColors = setupButton("resetColors", 0, vPosControls);
        SwitchButton opacityButton = setupSwitchButton(true, "solid", "transparent", 1, vPosControls++);
        Button recenterButton = setupButton("Recenter", 0, vPosControls);
        ag.followDrawSpotButton = setupSwitchButton(false, "fixed camera", "follow draw spot", 1, vPosControls++);
        Button newInstanceButton = setupButton(getNewInstanceButtonLabel(), 0, vPosControls);
        SwitchButton showStatsButton = setupSwitchButton(false, "hide stats", "show stats", 1, vPosControls++);
        SwitchButton searchCanvasButton = setupSwitchButton(false, "show searched nodes", "hide searched nodes", 0, vPosControls);
        SwitchButton solveCanvasButton = setupSwitchButton(false, "show solution", "hide solution", 1, vPosControls++);
        print.setOnAction(e -> print());
        delete.setOnAction(e -> {
            final var deletionMessage = OutputController.deleteOutputFiles();
            PopUpController.showPopupMessage(deletionMessage, true, m.getPrimaryStage());
        });
        final var timeline = m.getTimeline();
        restart.setOnAction(e -> restart(timeline));
        searchCanvasButton.getMultipleEventsHandler()
                .addEventHandler(e -> m.getSearchCanvas().setOpacity(searchCanvasButton.isActive() ? 0 : 1));
        solveCanvasButton.getMultipleEventsHandler()
                .addEventHandler(e -> m.getSolutionCanvas().setOpacity(solveCanvasButton.isActive() ? 0 : 1));
        pauseButton.setOnAction(e -> as.getPauseButtonUpdater().update(timeline.getStatus() == Animation.Status.RUNNING));
        opacityButton.getMultipleEventsHandler().addEventHandler(0, e -> this.setOpacity(opacityButton.isActive() ? 1 : OPACITY_VALUE));
        showStatsButton.getMultipleEventsHandler().addEventHandler(0, e -> statsCanvas.setVisible(!showStatsButton.isActive()));
        ag.followDrawSpotButton.getMultipleEventsHandler().addEventHandler(e -> {
            if (ag.followDrawSpotButton.isActive()) {
                as.getSpeedSliderUpdater().updateSliders(0, 0, 0);
            }
            rotateSliderDisabler.update(ag.followDrawSpotButton.isActive());
        });
        newInstanceButton.setOnAction(e -> createNewInstance(newInstanceButton));
        recenterButton.setOnAction(e -> {
            Canvas canvas = m.getBgCanvas();
            canvas.setRotate(0);
            canvas.setTranslateX(0);
            canvas.setTranslateY(0);
            sliderUpdater.updateSliders(0, 0, 0);
            ag.followDrawSpotButton.setActive(false);
            rotateSliderDisabler.update(false);
        });
        final String activeStyleCss =
                "-fx-background-color: lightgreen;" +
                "-fx-text-fill: darkred";/* +
                        "-fx-border-color: darkred"*/
        pauseButton.setActiveStyle(activeStyleCss);
        as.setPauseButtonUpdater(isActive -> togglePausePlay(pauseButton, timeline, isActive));
        as.setRestartButtonUpdater(isActive -> restart.setStyle(isActive ? activeStyleCss : ""));
        resetColors.setOnAction(e -> resetColors());
        newInstanceButton.setOnAction(e -> createNewInstance(newInstanceButton));
    }

    private static void togglePausePlay(SwitchButton pauseButton, Timeline timeline, boolean isActive) {
        pauseButton.setActive(isActive);
        if (pauseButton.isActive()) {
            timeline.pause();
        } else {
            timeline.play();
        }
    }

    private void resetColors() {
        c.resetColors();
        ag.drawMazeWalls(m.getBgCanvas(), 0);
        m.getBackgroundUpdater().accept(c.getBackGroundColor());
    }

    private static void createNewInstance(Button newInstanceButton) {
        if (AppManager.getInstances() < MAX_NUMBER_OF_INSTANCES) {
            new AppManager(new Stage()).start();
        } else {
            newInstanceButton.setDisable(true);
            newInstanceButton.setText(getNewInstanceButtonLabel());
        }
    }

    private void print() {
        OutputController.createOutputImagesOfMazeList(ag.getMazeList(), as.nodesToGoal);
        PopUpController.showPopupMessage(OutputController.getOutputImageMessage(ag.getMazeList()), false, m.getPrimaryStage());
        final var lineSeparator = System.lineSeparator();
        LOGGER.info("{}{}", lineSeparator, AsciiArt.LOOK_IN_OUTPUT);
    }

    private void restart(Timeline timeline) {
        m.restartSim();
        timeline.play();
    }

    private static String getNewInstanceButtonLabel() {
        return AppManager.getInstances() < MAX_NUMBER_OF_INSTANCES ? "start new instance" : "Maximum reached";
    }

    private Button setupButton(String text, int hPos, int vPos) {
        Button button = new Button(text);
        button.setPrefWidth(PREF_WIDTH_CONTROLS / 2.);
        button.setCursor(Cursor.HAND);
        this.add(button, hPos, vPos, 1, 1);
        return button;
    }

    private SwitchButton setupSwitchButton(boolean on, String onText, String offText, int hPos, int vPos) {
        SwitchButton switchButton = new SwitchButton(on, onText, offText);
        switchButton.setPrefWidth(PREF_WIDTH_CONTROLS / 2.);
        switchButton.setMinWidth(PREF_WIDTH_CONTROLS / 2.);
        switchButton.getMultipleEventsHandler().addEventHandler(e -> m.updateFrameIfPaused());
        this.add(switchButton, hPos, vPos);
        return switchButton;
    }

    @SuppressWarnings("all")
    private List<MazeDimension> getMazeDimensionList(String label, int min, int interval, int size) {
        List<MazeDimension> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            int length = (int) (min + i * interval);
            list.add(new MazeDimension(label, MazeGenerator.oddThreeMinimum(length)));
        }
        return list;
    }

    private void setupDropdownMenuInputMaze() {
        final ComboBox<String> menu = new ComboBox<>();
        setComboBox(menu, vPosControls++);
        List<String> list = new ArrayList<>(Arrays.asList(Objects.requireNonNull(new File(InputController.INPUT_DIRECTORY).list())));
        list.add(GENERATION_MODE);
        for (String option : list) {
            menu.getItems().add(option);
        }
        menu.setValue(GENERATION_MODE);
        menu.setOnAction(e -> selectInputMaze(menu.getValue(), m.getBgCanvas()));
        as.setModeDropdownMenuUpdater(() -> menu.setValue(GENERATION_MODE));
    }

    private void selectInputMaze(String selected, Canvas canvas) {
        if (selected.equals(GENERATION_MODE)) {
            m.start();
        } else {
            setupSolveForLoadedMaze(selected, canvas);
        }
    }

    private void setupSolveForLoadedMaze(String selected, Canvas canvas) {
        m.stopSim();
        m.reset();
        Maze loadedMaze = InputController.loadInputMaze(selected, new File(InputController.INPUT_DIRECTORY + "/" + selected));
        ag.addMazeToMazeList(loadedMaze);
        int mazeWidth = loadedMaze.getWidth();
        int mazeHeight = loadedMaze.getHeight();
        ag.mazeSize = new Dimension(mazeWidth, mazeHeight);
        setCanvasSize(canvas, mazeWidth, mazeHeight);
        ag.mazeUnitSize = canvas.getHeight() / mazeHeight;
        mazeWidthMenu.setValue(new MazeDimension(MAZE_WIDTH_LABEL, mazeWidth));
        mazeHeightMenu.setValue(new MazeDimension(MAZE_HEIGHT_LABEL, mazeHeight));
        ag.drawMazeWalls(m.getBgCanvas(), ag.mazeAngle.get());
        m.drawLoadedMaze();
        as.setupSolve();
        as.getPauseButtonUpdater().update(true);
        LOGGER.info(AsciiArt.ANSI_GREEN + "Press play to solve the maze" + AsciiArt.ANSI_RESET);
    }

    private void setupDropdownMenuGenerationMethods() {
        final ComboBox<MazeCreatorEnum> generationMethods = new ComboBox<>();
        setComboBox(generationMethods, vPosControls++);
        for (MazeCreatorEnum generationMethod : EnumSet.allOf(MazeCreatorEnum.class)) {
            generationMethods.getItems().add(generationMethod);
        }
        generationMethods.setValue(ag.getMazeGenPicker());
        generationMethods.setOnAction(e -> ag.setMazeGenPicker(generationMethods.getValue()));
    }


    private static <T> StringConverter<T> getStringConverter() {
        return new StringConverter<>() {
            @Override
            public String toString(T item) {
                return switch (item) {
                    case MazeCreatorEnum mazeCreatorEnum -> mazeCreatorEnum.getName();
                    case MazeDimension mazeDimension -> mazeDimension.label;
                    case null -> "null";
                    default -> item.toString();
                };
            }

            @Override
            public T fromString(String id) {
                return null;
            }
        };
    }

    private void setupDropdownMenuSolveMethods() {
        final ComboBox<String> solveMethods = new ComboBox<>();
        solveMethods.setId("solve-methods-combobox");
        setComboBox(solveMethods, vPosControls++);
        List<String> list = getSolveMethodList();
        for (String string : list) {
            solveMethods.getItems().add(string);
        }
        solveMethods.setValue(as.getSolveSelector());
        solveMethods.setOnAction(e -> as.setSolveSelector(solveMethods.getValue()));
    }

    public SliderUpdater getSliderUpdater() {
        return sliderUpdater;
    }

    private record MazeDimension(String label, int length) {

            private MazeDimension(String label, int length) {
                this.length = length;
                this.label = label + length;
            }
        }

    private void setupDropdownMenuMazeWidth() {
        setComboBox(mazeWidthMenu, vPosControls++);
        List<MazeDimension> mazeDimensionList = getMazeDimensionList(MAZE_WIDTH_LABEL, 21, 20, 60);
        for (MazeDimension d : mazeDimensionList) {
            mazeWidthMenu.getItems().add(d);
        }
        mazeWidthMenu.setValue(new MazeDimension(MAZE_WIDTH_LABEL, ag.mazeSize.width));
        mazeWidthMenu.setOnAction(e -> setMazeWidth(mazeWidthMenu.getValue().length));
    }

    private void setMazeWidth(int width) {
        m.getTimeline().stop();
        int height = ag.mazeSize.height;
        setCanvasSize(m.getBgCanvas(), width, height);
        ag.mazeSize = new Dimension(width, height);
        m.restartSim();
    }

    private void setupDropdownMenuMazeHeight() {
        setComboBox(mazeHeightMenu, vPosControls++);
        List<MazeDimension> mazeDimensionList = getMazeDimensionList(MAZE_HEIGHT_LABEL, 21, 20, 40);
        for (MazeDimension d : mazeDimensionList) {
            mazeHeightMenu.getItems().add(d);
        }
        mazeHeightMenu.setValue(new MazeDimension(MAZE_HEIGHT_LABEL, ag.mazeSize.height));
        mazeHeightMenu.setOnAction(e -> setMazeHeight(mazeHeightMenu.getValue().length));
    }

    private void setMazeHeight(int height) {
        m.getTimeline().stop();
        int width = ag.mazeSize.width;
        setCanvasSize(m.getBgCanvas(), width, height);
        ag.mazeSize = new Dimension(width, height);
        m.restartSim();
    }

    private static void setCanvasSize(Canvas canvas, int mazeWidth, int mazeHeight) {
        if (mazeWidth > mazeHeight) {
            canvas.setWidth(AppConstants.CANVAS_LONG_SIDE);
            canvas.setHeight(AppConstants.CANVAS_LONG_SIDE * mazeHeight / (double) mazeWidth);
        } else {
            canvas.setWidth(AppConstants.CANVAS_LONG_SIDE * mazeWidth / (double) mazeHeight);
            canvas.setHeight(AppConstants.CANVAS_LONG_SIDE);
        }
    }

    private void setupDropdownMenuImperfect() {
        final String imperfect = "Imperfect: ";
        final String percentSign = " %";
        final ComboBox<String> menu = new ComboBox<>();
        setComboBox(menu, vPosControls++);
        for (int i = 0; i <= 40; i++) {
            menu.getItems().add(imperfect + i + percentSign);
        }
        menu.setValue(imperfect + ag.imperfectPercentage + percentSign);
        menu.setOnAction(e ->
                ag.imperfectPercentage =
                        Integer.parseInt(menu.getValue().substring(imperfect.length(), menu.getValue().length() - percentSign.length())));
    }

    private <T> void setComboBox(ComboBox<T> comboBox, int vPos) {
        comboBox.setPrefWidth(PREF_WIDTH_CONTROLS);
        comboBox.setConverter(getStringConverter());
        this.add(comboBox, 0, vPos, 2, 1);
    }

    private static List<String> getSolveMethodList() {
        return List.of(AppConstants.DIJKSTRA, AppConstants.BREADTH_FIRST_SEARCH, AppConstants.DEPTH_FIRST_SEARCH, AppConstants.A_STAR);
    }

    private void setupColorPickers() {
        setupWallColorPickers();
        setupPathColorPickers();
        setupSolutionColorPickers();
        setupOtherColorPickers();
        c.colorUpdater = () -> {
            wallColorPicker.setValue(c.wallColor);
            bWallColorPicker.setValue(c.bottomWallColor);
            pathColorPicker.setValue(c.pathColor);
            bPathColorPicker.setValue(c.bottomPathColor);
            beginSolveCp.setValue(c.beginSolutionColor);
            endSolveCp.setValue(c.endSolutionColor);
            nodeColorPicker.setValue(c.nodeColor);
            backGroundColorPicker.setValue(c.backGroundColor);
        };
    }


    private void setupWallColorPickers() {
        wallColorPicker = setupColorPicker(c.wallColor, 0, vPosControls);
        bWallColorPicker = setupColorPicker(c.bottomWallColor, 1, vPosControls++);
        wallColorPicker.setOnAction(e -> {
            c.wallColor = wallColorPicker.getValue();
            ag.drawMazeWalls(m.getBgCanvas(), ag.mazeAngle.get());
        });
        bWallColorPicker.setOnAction(e -> {
            c.bottomWallColor = bWallColorPicker.getValue();
            ag.drawMazeWalls(m.getBgCanvas(), ag.mazeAngle.get());
        });
    }

    private void setupPathColorPickers() {
        pathColorPicker = setupColorPicker(c.getPathColor(), 0, vPosControls);
        bPathColorPicker = setupColorPicker(c.getBPathColor(), 1, vPosControls++);
        pathColorPicker.setOnAction(e -> c.pathColor = (pathColorPicker.getValue()));
        bPathColorPicker.setOnAction(e -> c.bottomPathColor = (bPathColorPicker.getValue()));
    }

    private void setupSolutionColorPickers() {
        beginSolveCp = setupColorPicker(c.beginSolutionColor, 0, vPosControls);
        endSolveCp = setupColorPicker(c.endSolutionColor, 1, vPosControls++);
        beginSolveCp.setOnAction(e -> c.beginSolutionColor = (beginSolveCp.getValue()));
        endSolveCp.setOnAction(e -> c.endSolutionColor = (endSolveCp.getValue()));
    }

    private void setupOtherColorPickers() {
        nodeColorPicker = setupColorPicker(c.nodeColor, 0, vPosControls);
        backGroundColorPicker = setupColorPicker(c.backGroundColor, 1, vPosControls++);
        nodeColorPicker.setOnAction(e -> c.nodeColor = nodeColorPicker.getValue());
        backGroundColorPicker.setOnAction(e -> {
            m.getBackgroundUpdater().accept(backGroundColorPicker.getValue());
            bcBrightnessSlider.setValue(backGroundColorPicker.getValue().getBrightness());
        });
    }

    private ColorPicker setupColorPicker(Color startColor, int hPos, int vPos) {
        ColorPicker colorPicker = new ColorPicker(startColor);
        colorPicker.setPrefWidth(PREF_WIDTH_CONTROLS / 2.);
        colorPicker.setStyle("-fx-color-label-visible: false ;");
        this.add(colorPicker, hPos, vPos, 1, 1);
        return colorPicker;
    }

    private void setupSliders(Canvas bgCanvas) {
        Slider simSpeed = setupSlider(new HorSliderInput(2, 10000, AppConstants.INIT_FRAMERATE, 0, vPosControls++, 2), PREF_WIDTH_CONTROLS, true);
        Slider rotateCanvasSlider = setupSlider(new HorSliderInput(-180, 180, bgCanvas.getRotate(),
                0, vPosControls++, 2), PREF_WIDTH_CONTROLS, false);
        Slider zoomSlider = setupSlider(new HorSliderInput(AppConstants.MIN_SCALE_FACTOR, AppConstants.MAX_SCALE_FACTOR,
                m.getBgCanvas().getScaleX(), 0, vPosControls, 2), PREF_WIDTH_CONTROLS, false);
        bcBrightnessSlider = setupSlider(new HorSliderInput(0.01,
                1, ((Color) m.getBackgroundFill().getFill()).getBrightness(), 2, vPosControls++, 2), AppConstants.SCREEN_SIZE.width, false);
        /*acos(ag.lg.getStartX())*/
        Slider bgSlider = setupSlider(new HorSliderInput(-Math.PI, Math.PI, 0,
                2, vPosControls++, 2), AppConstants.SCREEN_SIZE.width, false);
        Slider horPosSlider = setupSlider(new HorSliderInput(-m.getScene().getWidth(), m.getScene().getWidth(), bgCanvas.getTranslateX(),
                2, vPosControls++, 2), AppConstants.SCREEN_SIZE.width, false);
        Slider verPosSlider = setupVerSlider(bgCanvas);
        //fullCycleDuration = max.double
        simSpeed.valueProperty()
                .addListener((oldVal, curVal, newVal) ->
                        m.getTimeline().setRate(newVal.doubleValue() / AppConstants.INIT_FRAMERATE));

        bcBrightnessSlider.valueProperty().addListener((o, cur, n) -> {
            Color curColor = (Color) m.getBackgroundFill().getFill();
            Color newColor = Color.hsb(curColor.getHue(), curColor.getSaturation(), n.doubleValue());
            m.getBackgroundUpdater().accept(newColor);
            backGroundColorPicker.setValue(newColor);
        });
        bgCanvas.rotateProperty().bind(rotateCanvasSlider.valueProperty());
        bgCanvas.scaleXProperty().bind(zoomSlider.valueProperty());
        bgCanvas.scaleYProperty().bind(zoomSlider.valueProperty());
        bgCanvas.translateZProperty().bind(zoomSlider.valueProperty().multiply(100));
        zoomSlider.valueProperty()
                .addListener((oldVal, curVal, newVal) ->
                        updatePositionSliders(horPosSlider, verPosSlider, bgCanvas, curVal.doubleValue(), newVal.doubleValue()));

        bgSlider.valueProperty().addListener((oldVal, curVal, newVal) -> {
            double angle = newVal.doubleValue();
            ag.drawMazeWalls(m.getBgCanvas(), angle);
        });

        horPosSlider.valueProperty().addListener((oldVal, curVal, newVal) -> bgCanvas.setTranslateX(-newVal.doubleValue()));
        verPosSlider.valueProperty().addListener((oldVal, curVal, newVal) -> bgCanvas.setTranslateY(newVal.doubleValue()));
        as.setSpeedSliderUpdater((cur, max, var3) -> updateSpeed(simSpeed, cur, max));
        sliderUpdater = (x, y, angle) -> {
            horPosSlider.setValue(x);
            verPosSlider.setValue(y);
            rotateCanvasSlider.setValue(angle);
        };
        m.setZoomSliderUpdater((zoom, var2, var3) -> zoomSlider.setValue(zoom));
        rotateSliderDisabler = rotateCanvasSlider::setDisable;
    }

    private void updateSpeed(Slider simSpeed, double cur, double max) {
        if (!ag.followDrawSpotButton.isActive()) {
            simSpeed.setValue(cur);
        }
        if (Double.compare(cur, 0.0) == 0) {
            simSpeed.setValue(5. / AppConstants.INIT_FRAMERATE);
        }
        if (Double.compare(max, 0.0) != 0) {
            simSpeed.setMax(max);
        }
    }

    private static void updatePositionSliders(Slider horPosSlider, Slider verPosSlider,
                                              Canvas canvas, double curVal, double newVal) {
        final var ratio = newVal / curVal;
        horPosSlider.setMin(horPosSlider.getMin() * ratio);
        horPosSlider.setMax(horPosSlider.getMax() * ratio);
        horPosSlider.setValue(-canvas.getTranslateX() * ratio);
        verPosSlider.setMin(verPosSlider.getMin() * ratio);
        verPosSlider.setMax(verPosSlider.getMax() * ratio);
        verPosSlider.setValue(canvas.getTranslateY() * ratio);
    }

    private record HorSliderInput(double minValue, double maxValue, double curValue, int hPos, int vPos, int hSpan) {
    }

    private Slider setupSlider(HorSliderInput i, int prefWidth, boolean setScale) {
        Slider slider = new Slider(i.minValue, i.maxValue, i.curValue);
        slider.setPrefWidth(prefWidth);
        slider.setShowTickLabels(setScale);
        slider.setShowTickMarks(setScale);
        slider.setMajorTickUnit(50);
        slider.setMinorTickCount(5);
        slider.setBlockIncrement(10);
        this.add(slider, i.hPos, i.vPos, i.hSpan, 1);
        return slider;
    }

    private Slider setupVerSlider(Canvas bgCanvas) {
        Slider slider = new Slider(-m.getScene().getHeight(), m.getScene().getHeight(), bgCanvas.getTranslateY());
        slider.setPrefHeight(AppConstants.SCREEN_SIZE.height);
        slider.setOrientation(Orientation.VERTICAL);
        this.add(slider, 6, 0, 1, vPosControls);
        return slider;
    }
}
