package hzt.view;

import hzt.controller.AppManager;
import hzt.model.custom_controls.SwitchButton;
import hzt.model.maze.Maze;
import hzt.model.maze.Pixel;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.LinearGradient;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import static hzt.controller.AppConstants.INIT_CANVAS_SIZE;

class GenVars {

    private static final int INIT_MAZE_HEIGHT = 61;
    private static final Dimension INIT_MAZE_SIZE = new Dimension(
            INIT_MAZE_HEIGHT * INIT_CANVAS_SIZE.width / INIT_CANVAS_SIZE.height,
            INIT_MAZE_HEIGHT); // nog naar kijken
    final AppManager m;
    final ColorController c;
    private final List<Maze> mazeList;

    // animation constants

    SwitchButton followDrawSpotButton;
    Pixel[][] mazePixels;
    int genSolveCounter;
    int extraKnockdown;
    int totalCells;
    int visited;
    int nrOfCreatedPaths;
    int entranceX;
    int exitX;
    int frameCount = 0;

    double mazeUnitSize;
    double imperfectPercentage;
    double startTimeGen;
    double stopTimeGen;

    final DoubleProperty mazeAngle = new SimpleDoubleProperty();
    Dimension mazeSize = INIT_MAZE_SIZE;

    GenVars(AppManager m, ColorController c) {
        this.m = m;
        this.c = c;
        this.mazeList = new ArrayList<>();
    }

    List<Maze> getMazeList() {
        return List.copyOf(mazeList);
    }

    public Maze getCurrentMaze() {
        return mazeList.get(0);
    }

    void addMazeToMazeList(Maze maze) {
        this.mazeList.add(0, maze);
    }

    public double getMazeUnitSize() {
        return mazeUnitSize;
    }

    public Dimension getMazeSize() {
        return mazeSize;
    }
}
