package hzt.view;

import hzt.controller.AppManager;
import hzt.model.graph.Node;
import hzt.model.maze.Pixel;

import java.util.LinkedList;
import java.util.List;

import static java.lang.System.nanoTime;

public abstract class SolveVars {

    final AppManager m;
    final AnimateGen ag;
    final ColorController c;

    // animation vars
    boolean solutionDrawing;

    int solutionEntryNumber;
    int solutionLength;

    double startTimeSolve;
    double stopTimeSolve;

    final List<Node> nodesToGoal = new LinkedList<>();
    final List<Pixel> solutionEntryList = new LinkedList<>();

    Node source;
    Node goal;

    SolveVars(AppManager m, AnimateGen ag, ColorController c) {
        this.m = m;
        this.ag = ag;
        this.c = c;
    }

    double getSolveTime() {
        return ((Double.compare(stopTimeSolve, 0) == 0 ? stopTimeSolve : nanoTime()) - startTimeSolve) / 1e9;
    }

    public List<Node> getNodesToGoal() {
        return List.copyOf(nodesToGoal);
    }
}
