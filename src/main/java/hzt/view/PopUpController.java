package hzt.view;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.util.Duration;

final class PopUpController {

    private PopUpController() {
    }

    private static Popup createPopup(final String message, boolean warning) {
        final Popup popup = new Popup();
        popup.setAutoFix(true);
        popup.setAutoHide(true);
        popup.setHideOnEscape(true);
        Label label = new Label(message);
        label.setPrefHeight(10);
        label.setOnMouseReleased(e -> popup.hide());
        label.getStylesheets().add("/static/css/styles.css");
        String styleClass = warning ? "popup_warning" : "popup";
        label.getStyleClass().add(styleClass);
        popup.getContent().add(label);
        return popup;
    }

    static void showPopupMessage(final String message, boolean warning, Stage stage) {
        final Popup popup = createPopup(message, warning);
        popup.setOnShown(e -> {
            popup.setX(stage.getX() + stage.getWidth() / 2 - popup.getWidth() / 2);
            popup.setY(stage.getY() + stage.getHeight() / 2 - popup.getHeight() / 2);
        });
        popup.show(stage);
        new Timeline(new KeyFrame(Duration.millis(1400), e -> popup.hide())).play();
    }
}
