package hzt;

import hzt.controller.AppManager;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Launcher extends Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Launcher.class);

    public static void main(String[] args) {
        try {
            launch(args);
        } catch (Exception e) {
            LOGGER.error("Application will stop now", e);
            Platform.exit();
        }
    }

    @Override
    public void start(Stage stage) {
        new AppManager(stage).start();
    }
}
