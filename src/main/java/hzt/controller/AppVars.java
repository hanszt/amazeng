package hzt.controller;

import hzt.view.ColorController;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import static hzt.controller.AppConstants.*;
import static javafx.util.Duration.seconds;

public abstract class AppVars {

    final ColorController c;
    final Stage primaryStage;
    final Scene scene;
    final Pane root;
    final Canvas mazeCanvas;
    final Canvas bgCanvas;
    final Canvas solutionCanvas;
    final Canvas searchCanvas;
    final Timeline timeline;

    boolean runSolve;

    int genSolveCounter;
    private int frameCount = 0;
    private double stopTimeSim;

    double runTime;
    double startTimeSim;
    double tStart;
    double tStop;

    BackgroundFill backgroundFill;

    protected AppVars(ColorController c, Stage primaryStage) {
        this.c = c;
        this.primaryStage = primaryStage;
        this.root = new StackPane();
        root.setId("root");
        this.scene = new Scene(root, INIT_SCENE_SIZE.width, INIT_SCENE_SIZE.height);
        this.bgCanvas = setupCanvas(scene);
        this.mazeCanvas = setupCanvas(scene);
        this.solutionCanvas = setupCanvas(scene);
        this.searchCanvas = setupCanvas(scene);
        this.timeline = new Timeline(new KeyFrame(seconds(1. / INIT_FRAMERATE), event -> manageSim(mazeCanvas, searchCanvas, solutionCanvas)));
        timeline.setCycleCount(Animation.INDEFINITE);
        this.backgroundFill = new BackgroundFill(this.c.getBackGroundColor(), CornerRadii.EMPTY, Insets.EMPTY);
        setStageProperties();
    }

    private void setStageProperties() {
        primaryStage.setScene(scene);
        primaryStage.setMinWidth(400);
        primaryStage.setMinHeight(400);
    }

    abstract void manageSim(Canvas mazeCanvas, Canvas searchCanvas, Canvas solutionCanvas);

    private static Canvas setupCanvas(Scene scene) {
        Canvas canvas = new Canvas(INIT_CANVAS_SIZE.width, INIT_CANVAS_SIZE.height);
        canvas.setScaleX(scene.getWidth() / canvas.getWidth());
        canvas.setScaleY(scene.getHeight() / canvas.getHeight());
        return canvas;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public Scene getScene() {
        return scene;
    }

    public Timeline getTimeline() {
        return timeline;
    }

    public Canvas getBgCanvas() {
        return bgCanvas;
    }

    public Canvas getSolutionCanvas() {
        return solutionCanvas;
    }

    public Canvas getSearchCanvas() {
        return searchCanvas;
    }

    public BackgroundFill getBackgroundFill() {
        return backgroundFill;
    }

    public double gettStop() {
        return tStop;
    }

    public void settStop(double tStop) {
        this.tStop = tStop;
    }

    public double getStopTimeSim() {
        return stopTimeSim;
    }

    public void setStopTimeSim(double stopTimeSim) {
        this.stopTimeSim = stopTimeSim;
    }

    public int getFrameCount() {
        return frameCount;
    }

    public void setFrameCount(int frameCount) {
        this.frameCount = frameCount;
    }
}
