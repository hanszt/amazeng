package hzt.controller;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

import java.awt.*;

import static java.lang.Math.sqrt;

public final class AppConstants {

    private AppConstants() {
    }

    // constants
    static final Rectangle2D PRIMARY_SCREEN_BOUNDS = Screen.getPrimary().getVisualBounds();
    public static final int MENU_WIDTH = 250;
    private static final double INIT_SCENE_HEIGHT = PRIMARY_SCREEN_BOUNDS.getHeight();
    // don't make the canvas to large or an exception will occur
    public static final int CANVAS_LONG_SIDE = 2000;
    private static final double GOLDEN_RATIO = (1 + sqrt(5)) / 2;

    public static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    public static final Dimension INIT_SCENE_SIZE = new Dimension((int) (INIT_SCENE_HEIGHT / GOLDEN_RATIO), (int) INIT_SCENE_HEIGHT);
    public static final Dimension INIT_CANVAS_SIZE = new Dimension((int) (CANVAS_LONG_SIDE / GOLDEN_RATIO), CANVAS_LONG_SIDE);

    public static final double INIT_FRAMERATE = 30; //f/s
    public static final double MIN_SCALE_FACTOR = 0.02;
    public static final double MAX_SCALE_FACTOR = 10;

    public static final String DOTTED_LINE = "----------------------------------------------------------------------------------------\n";
    static final String TITLE = "A'Maze'ng";
    public static final String DIJKSTRA = "Dijkstra";
    public static final String BREADTH_FIRST_SEARCH = "Breadth First Search";
    public static final String DEPTH_FIRST_SEARCH = "Depth First Search";
    public static final String A_STAR = "A*";
}
