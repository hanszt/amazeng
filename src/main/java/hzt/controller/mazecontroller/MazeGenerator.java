package hzt.controller.mazecontroller;

import hzt.model.maze.Pixel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public final class MazeGenerator {

    private final Random random;

    public MazeGenerator(Random random) {
        this.random = random;
    }

    public static Pixel[][] createGridWithMazeCells(int width, int height) {
        int widthOdd = oddThreeMinimum(width);
        int heightOdd = oddThreeMinimum(height);
        Pixel[][] mazePixels = new Pixel[widthOdd][heightOdd];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                mazePixels[x][y] = x % 2 == 0 || y % 2 == 0 ?
                        new Pixel(x, y, false) :
                        new Pixel(x, y, true);
            }
        }
        return mazePixels;
    }

    public static int oddThreeMinimum(int value) {
        return value % 2 == 0 ? (value + 1) : Math.max(value, 3);
    }

    public int getRandCellPos(int sideLength) {
        return random.nextInt((sideLength - 1) / 2) * 2 + 1;
    }

    public static List<Pixel> findClosedAdjCells(Pixel[][] mazePixels, Pixel currentCell) {
        List<Pixel> neighbourCellsWithAllWallsIntact = new ArrayList<>();
        int xWest = currentCell.getX() - 2;
        int xEast = currentCell.getX() + 2;
        int yNorth = currentCell.getY() - 2;
        int ySouth = currentCell.getY() + 2;
        if (yNorth >= 0) {
            Pixel northCell = mazePixels[currentCell.getX()][yNorth];
            if (allWallsIntact(mazePixels, northCell)) {
                neighbourCellsWithAllWallsIntact.add(northCell);
            }
        }
        if (xEast < mazePixels.length) {
            Pixel eastCell = mazePixels[xEast][currentCell.getY()];
            if (allWallsIntact(mazePixels, eastCell)) {
                neighbourCellsWithAllWallsIntact.add(eastCell);
            }
        }
        if (ySouth < mazePixels[0].length) {
            Pixel southCell = mazePixels[currentCell.getX()][ySouth];
            if (allWallsIntact(mazePixels, southCell)) {
                neighbourCellsWithAllWallsIntact.add(southCell);
            }
        }
        if (xWest >= 0) {
            Pixel westCell = mazePixels[xWest][currentCell.getY()];
            if (allWallsIntact(mazePixels, westCell)) {
                neighbourCellsWithAllWallsIntact.add(westCell);
            }
        }
        return neighbourCellsWithAllWallsIntact;
    }

    private static boolean allWallsIntact(Pixel[][] mazePixels, Pixel currentCell) {
        int xWest = currentCell.getX() - 1;
        int xEast = currentCell.getX() + 1;
        int yNorth = currentCell.getY() - 1;
        int ySouth = currentCell.getY() + 1;
        Pixel northPixel = mazePixels[currentCell.getX()][yNorth];
        Pixel eastPixel = mazePixels[xEast][currentCell.getY()];
        Pixel southPixel = mazePixels[currentCell.getX()][ySouth];
        Pixel westPixel = mazePixels[xWest][currentCell.getY()];
        return !northPixel.isPath() && !eastPixel.isPath() && !southPixel.isPath() && !westPixel.isPath();
    }

    public Pixel getRandAdjCell(List<Pixel> cells) {
        int selector = random.nextInt(cells.size());
        return cells.get(selector);
    }

    public static Pixel getFibAdjCell(List<Pixel> cells, int visited) {
        int fibNr = getFibonacci(visited);
        return cells.get(fibNr % cells.size());
    }

    /**
     * This function return nth Fibonacci number in Java
     *
     * @param n param
     * @return nth number in Fibonacci series
     */
    private static int getFibonacci(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        int nth = 1;
        int second = 1;
        int first = 0;
        for (int i = 2; i <= n; i++) {
            nth = first + second;
            first = second;
            second = nth;
        }
        if (nth < 0) {
            nth = 0;
        }
        return nth;
    }

    public static Pixel getModAdjCell(List<Pixel> cells, int visited) {
        return cells.get(visited % cells.size());
    }

    public static Pixel getVerticalAdjCell(List<Pixel> cells) {
        return cells.get(0);
    }

    public static Pixel getHorizontalAdjCell(List<Pixel> cells) {
        return cells.get(cells.size() - 1);
    }

    public static final class CustomShapePicker {
        private static int picker;
        private static int negPos;

        private CustomShapePicker() {
        }

        public static Pixel getAdjCellForSpiralShape(List<Pixel> cells, int visited) {
            Collections.sort(cells);
            if (picker == 2) {
                negPos = -1;
            }
            if (picker == 0) {
                negPos = 1;
            }
            if (visited % 15 == 0 && cells.size() >= 2) {
                picker = picker + negPos;
            }
            if (picker < cells.size()) {
                return cells.get(picker);
            } else if (picker - 1 < cells.size()) {
                return cells.get(picker - 1);
            } else {
                return cells.get(0);
            }
        }

        public static Pixel getTestAdjCell(List<Pixel> cells, int visited) {
            if (picker > 2) {
                picker = 0;
            }
            if (visited % 60 == 0 && cells.size() >= 2) {
                picker++;
            }
            if (picker < cells.size()) {
                return cells.get(picker);
            } else {
                return cells.get(0);
            }
        }
    }
}

