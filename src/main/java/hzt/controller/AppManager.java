package hzt.controller;

import hzt.model.data.AsciiArt;
import hzt.model.maze.Pixel;
import hzt.utils.SliderUpdater;
import hzt.view.AnimateGen;
import hzt.view.AnimateSolve;
import hzt.view.ColorController;
import hzt.view.StatsController;
import hzt.view.UserInputPane;
import javafx.animation.Animation;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.function.Consumer;

public class AppManager extends AppVars {

    private static final int ANNOUNCEMENT_FONT_SIZE = 250;
    private static final Logger LOGGER = LoggerFactory.getLogger(AppManager.class);

    private static int instances = 0;
    private final int instance;
    private final AnimateGen ag = new AnimateGen(this, c);
    private final AnimateSolve as = new AnimateSolve(this, ag, c);
    private final StatsController s = new StatsController(this, ag, as);
    private final UserInputPane up = new UserInputPane(this, ag, as, c);
    private Consumer<Color> backgroundUpdater;
    private SliderUpdater zoomSliderUpdater;
    private Point2D initPoint;

    public AppManager(Stage primaryStage) {
        super(new ColorController(), primaryStage);
        instance = ++instances;
        primaryStage.setTitle(String.format("%s (%d)", AppConstants.TITLE, instance));
        primaryStage.setOnCloseRequest(e -> printClosingText());
        setAppProperties();
    }

    final void setAppProperties() {
        bindAllCanvas(bgCanvas, mazeCanvas, searchCanvas, solutionCanvas);
        backgroundUpdater = color -> {
            backgroundFill = new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY);
            root.setBackground(new Background(backgroundFill));
        };
        backgroundUpdater.accept(c.getBackGroundColor());
        root.getChildren().addAll(bgCanvas, mazeCanvas, searchCanvas, solutionCanvas/*, get3DBox()*/, up);
        addResponsiveSceneMovement(scene, bgCanvas);
    }

    private static void bindAllCanvas(Canvas bgCanvas, Canvas... others) {
        for (Canvas other : others) {
            bindCanvasToBgCanvas(other, bgCanvas);
        }
    }

    private static void bindCanvasToBgCanvas(Canvas canvas, Canvas bgCanvas) {
        canvas.widthProperty().bind(bgCanvas.widthProperty());
        canvas.heightProperty().bind(bgCanvas.heightProperty());
        canvas.scaleXProperty().bind(bgCanvas.scaleXProperty());
        canvas.scaleYProperty().bind(bgCanvas.scaleYProperty());
        canvas.translateXProperty().bind(bgCanvas.translateXProperty());
        canvas.translateYProperty().bind(bgCanvas.translateYProperty());
        canvas.translateZProperty().bind(bgCanvas.translateZProperty());
        canvas.rotateProperty().bind(bgCanvas.rotateProperty());
        canvas.rotationAxisProperty().bind(bgCanvas.rotationAxisProperty());
    }

    private void addResponsiveSceneMovement(Scene scene, Canvas canvas) {
        scene.setOnMousePressed(e -> initPoint = new Point2D(
                canvas.getTranslateX() - e.getX(),
                canvas.getTranslateY() - e.getY()));
        scene.setOnMouseDragged(e -> {
            Point2D newOfSet = new Point2D(e.getX() + initPoint.getX(), e.getY() + initPoint.getY());
            //first update the sliders, and then the actual parameters in sim. Then there will not be an interaction!
            up.getSliderUpdater().updateSliders(-newOfSet.getX(), newOfSet.getY(), mazeCanvas.getRotate());
            canvas.setTranslateX(newOfSet.getX());
            canvas.setTranslateY(newOfSet.getY());
        });
        addMouseScrolling(canvas);
    }

    private void addMouseScrolling(Canvas canvas) {
        Parent root = scene.getRoot();
        root.setOnScroll(e -> zoom(canvas, e));
    }

    private void zoom(Canvas canvas, ScrollEvent e) {
        // Adjust the zoom factor as per your requirement
        double zoomFactor = 1;
        final var scaleX = canvas.getScaleX();
        final var deltaY = e.getDeltaY();
        if (scaleX > AppConstants.MIN_SCALE_FACTOR && deltaY < 0) {
            zoomFactor = 0.9;
        }
        if (scaleX < AppConstants.MAX_SCALE_FACTOR && deltaY > 0) {
            zoomFactor = 1.1;
        }
        zoomSliderUpdater.updateSliders(scaleX * zoomFactor, 0, 0);
    }

    public void start() {
        primaryStage.show();
        runTime = System.nanoTime();
        LOGGER.info("Simulation of instance {} starting...", instance);
        startTimeSim = System.nanoTime();
        ag.initializeGenVars(as, mazeCanvas);
        drawStartScreen(bgCanvas, mazeCanvas);
        initializeTimeLine();
    }

    public void updateFrameIfPaused() {
        if (Objects.equals(getTimeline().getStatus(), Animation.Status.PAUSED)) {
            manageSim(mazeCanvas, searchCanvas, solutionCanvas);
        }
    }

    private void initializeTimeLine() {
        as.getSpeedSliderUpdater().updateSliders(ag.getMazeSize().height, ag.getMazeSize().height * 16, 0);
        tStart = System.nanoTime();
        as.getPauseButtonUpdater().update(false);
        timeline.play();
    }

    private void drawStartScreen(Canvas wallsCanvas, Canvas mazeCanvas) {
        ag.drawMazeWalls(wallsCanvas, 0);
        GraphicsContext gc = mazeCanvas.getGraphicsContext2D();
        gc.clearRect(0, 0, mazeCanvas.getWidth(), mazeCanvas.getHeight());
        gc.setStroke(Color.YELLOW);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setLineWidth(10);
        gc.setFont(new Font("", ANNOUNCEMENT_FONT_SIZE));
        gc.strokeText(AppConstants.TITLE /*+ "Click to Start"*/, mazeCanvas.getWidth() / 2, mazeCanvas.getHeight() / 2);
        System.out.printf("%n%n" + AppConstants.DOTTED_LINE + "%n" + AsciiArt.TITLE_FX);
        System.out.printf("This program generates and solves mazes. %n" +
                "A simulation of the generation and solve process is provided.%n%n" +
                "Give it a try to solve the generated maze or continue to see the solution.. ಠᴗಠ%nEnjoy!%n" + AppConstants.DOTTED_LINE);
    }

    void manageSim(Canvas mazeCanvas, Canvas searchCanvas, Canvas solutionCanvas) {
        try {
            if (System.nanoTime() - tStart > 2e9 && !runSolve) {
                ag.manageMazeGeneration(s, mazeCanvas);
            }
            if (runSolve) {
                as.manageMazeSolve(s, searchCanvas, solutionCanvas);
            }
        } catch (OutOfMemoryError e) {
            LOGGER.error("The process takes up to much memory.%nThe program will close now.", e);
            closeStage();
        }
    }

    public void followDrawSpot(Pixel currentDrawSpot) {
        double translationX = (ag.getMazeSize().width / 2. - currentDrawSpot.getX()) * ag.getMazeUnitSize() * mazeCanvas.getScaleX();
        double translationY = (ag.getMazeSize().height / 2. - currentDrawSpot.getY()) * ag.getMazeUnitSize() * mazeCanvas.getScaleY();
        up.getSliderUpdater().updateSliders(-translationX, translationY, 0);
    }

    public void drawLoadedMaze() {
        GraphicsContext gc = mazeCanvas.getGraphicsContext2D();
        gc.clearRect(0, 0, mazeCanvas.getWidth(), mazeCanvas.getHeight());
        System.out.println("Drawing loaded maze...");
        double startTime = System.nanoTime();
        double unitSize = ag.getMazeUnitSize();
        Pixel[][] pixelArrays = ag.getCurrentMaze().getPixels();
        for (int y = 0; y < pixelArrays[0].length; y++) {
            for (Pixel[] pixelArray : pixelArrays) {
                if (pixelArray[y].isPath()) {
                    gc.setFill(c.getColorByHeight(c.getPathColor(), c.getBPathColor(), y, ag.getMazeSize().height));
                    gc.fillRect(pixelArray[y].getX() * unitSize, pixelArray[y].getY() * unitSize, unitSize, unitSize);
                }
            }
        }
        LOGGER.info("Loaded Maze drawn in %.2f seconds%n".formatted(System.nanoTime() - startTime / 1e9));
    }

    public void initiateSolveSim() {
        runSolve = true;
//        setTimelineRate(2); // with reference to initial framerate of 30 f/s
        as.getSpeedSliderUpdater().updateSliders(ag.getMazeSize().height, ag.getMazeSize().height * 8, 0);
    }

    public void restartSim() {
        as.getModeDropdownMenuUpdater().run();
        as.getRestartButtonUpdater().update(false);
        searchCanvas.getGraphicsContext2D().clearRect(0, 0, searchCanvas.getWidth(), searchCanvas.getHeight());
        solutionCanvas.getGraphicsContext2D().clearRect(0, 0, solutionCanvas.getWidth(), solutionCanvas.getHeight());
        runSolve = false;
        printRestartingText();
        timeline.stop();
        resetForRestart();
        start();
    }

    private void printRestartingText() {
        System.out.printf("restarting animation...%nAnimation Runtime: %.2f seconds%n%s%n",
                (System.nanoTime() - startTimeSim) / 1e9, AppConstants.DOTTED_LINE);
    }

    private void printClosingText() {
        System.out.printf("%s%nAnimation Runtime of instance %d: %.2f seconds%n%s%n", AsciiArt.CLOSING_MESSAGE,
                instance, (System.nanoTime() - runTime) / 1e9, AppConstants.DOTTED_LINE);
    }

    private void resetForRestart() {
        up.getSliderUpdater().updateSliders(0, 0, 0);
        reset();
    }

    public void reset() {
        tStart = tStop = startTimeSim = 0;
        genSolveCounter = 0;
        s.setInitStats(true);
    }

    public void stopSim() {
        runSolve = false;
        timeline.stop();
    }

    private void closeStage() {
        timeline.stop();
        primaryStage.close();
    }

    public static int getInstances() {
        return instances;
    }

    public StatsController getS() {
        return s;
    }

    public Consumer<Color> getBackgroundUpdater() {
        return backgroundUpdater;
    }

    public void setZoomSliderUpdater(SliderUpdater zoomSliderUpdater) {
        this.zoomSliderUpdater = zoomSliderUpdater;
    }
}

