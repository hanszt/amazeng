module AMazeng.FX {
    requires javafx.graphics;
    requires java.desktop;
    requires javafx.controls;
    requires org.slf4j;

    opens hzt to javafx.graphics;
    opens hzt.controller;
}
