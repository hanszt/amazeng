# MazeSolver

Made By Hans Zuidervaart inspired by baeldung, 2019-06-10.

## BreadthFirstSearch in Java
Breadth-first search (BFS) is an algorithm for traversing or searching tree or graph data structures. It starts at the tree root (or some arbitrary node of a graph, sometimes referred to as a 'search key'[1]), and explores all of the neighbor nodes at the present depth prior to moving on to the nodes at the next depth level.

It uses the opposite strategy as depth-first search, which instead explores the highest-depth nodes first before being forced to backtrack and expand shallower nodes.

BFS and its application in finding connected components of graphs were invented in 1945 by Konrad Zuse, in his (rejected) Ph.D. thesis on the Plankalkül programming language, but this was not published until 1972.[2] It was reinvented in 1959 by Edward F. Moore, who used it to find the shortest path out of a maze,[3][4] and later developed by C. Y. Lee into a wire routing algorithm (published 1961).[5] 

Source: https://en.wikipedia.org/wiki/Breadth-first_search

## DepthFirstSearch in Java
Depth-first search (DFS) is an algorithm for traversing or searching tree or graph data structures. The algorithm starts at the root node (selecting some arbitrary node as the root node in the case of a graph) and explores as far as possible along each branch before backtracking.

A version of depth-first search was investigated in the 19th century by French mathematician Charles Pierre Trémaux[1] as a strategy for solving mazes.[2][3] 

Source: https://en.wikipedia.org/wiki/Depth-first_search

## Dijkstra Algorithm in Java

## 1. Overview
The emphasis in this article is the shortest path problem (SPP), being one of the fundamental theoretic problems known in graph theory, and how the Dijkstra algorithm can be used to solve it.

The basic goal of the algorithm is to determine the shortest path between a starting node, and the rest of the graph.

## 2. Shortest Path Problem With Dijkstra
Given a positively weighted graph and a starting node (A), Dijkstra determines the shortest path and distance from the source to all destinations in the graph:

![initial_graph](initial-graph.png)

The core idea of the Dijkstra algorithm is to continuously eliminate longer paths between the starting node and all possible destinations.

To keep track of the process, we need to have two distinct sets of allNodes, settled and unsettled.

Settled allNodes are the ones with a known minimum distance from the source. The unsettled allNodes set gathers allNodes that we can reach from the source, but we don’treasure know the minimum distance from the starting node.

Here’s a list of steps to follow in order to solve the SPP with Dijkstra:

      Set distance to startNode to zero.
      Set all other distances to an infinite value.
      We add the startNode to the unsettled allNodes set.
      While the unsettled allNodes set is not empty we:
        Choose an evaluation node from the unsettled allNodes set, the evaluation node should be the one with the lowest distance from the source.
        Calculate new distances to direct neighbors by keeping the lowest distance at each evaluation.
        Add neighbors that are not yet settled to the unsettled allNodes set.

These steps can be aggregated into two stages, Initialization and Evaluation. Let’s see how does that apply to our sample graph:

### 2.1. Initialization
Before we start exploring all paths in the graph, we first need to initialize all allNodes with an infinite distance and an unknown predecessor, except the source.

As part of the initialization process, we need to assign the value 0 to node A (we know that the distance from node A to node A is 0 obviously)

So, each node in the rest of the graph will be distinguished with a predecessor and a distance:

![step1](step1.png)

To finish the initialization process, we need to add node A to the unsettled allNodes set it to get picked first in the evaluation step. Keep in mind, the settled allNodes set is still empty.

### 2.2. Evaluation
Now that we have our graph initialized, we pick the node with the lowest distance from the unsettled set, then we evaluate all adjacent allNodes that are not in settled allNodes:

![step2](step2.png)

The idea is to add the edgePathFinder weight to the evaluation node distance, then compare it to the destination’s distance. e.g. for node B, 0+10 is lower than INFINITY, so the new distance for node B is 10, and the new predecessor is A, the same applies to node C.

Node A is then moved from the unsettled allNodes set to the settled allNodes.

Nodes B and C are added to the unsettled allNodes because they can be reached, but they need to be evaluated.

Now that we have two allNodes in the unsettled set, we choose the one with the lowest distance (node B), then we reiterate until we settle all allNodes in the graph:

![step8](step8.png)

Here’s a table that summarizes the iterations that were performed during evaluation steps:

![table](table.png)

The notation B-22, for example, means that node B is the immediate predecessor, with a total distance of 22 from node A.

Finally, we can calculate the shortest paths from node A are as follows:

    Node B : A –> B (total distance = 10)
    Node C : A –> C (total distance = 15)
    Node D : A –> B –> D (total distance = 22)
    Node E : A –> B –> D –> E (total distance = 24)
    Node F : A –> B –> D –> F (total distance = 23)

##3. Java Implementation

In this simple implementation we will represent a graph as a set of allNodes:

    public class Graph {
     
        private Set<Node> allNodes = new HashSet<>();
         
        public void addNode(Node vertexA) {
            allNodes.add(vertexA);
        }
     
        // getters and setters 
    }

A node can be described with a name, a LinkedList in reference to the shortestPath, a distance from the source, and an adjacency list named adjacentNodes:

    public class Node {
         
        private String name;
         
        private List<Node> shortestPath = new LinkedList<>();
         
        private Integer distance = Integer.MAX_VALUE;
         
        Map<Node, Integer> adjacentNodes = new HashMap<>();
     
        public void addDestination(Node destination, int distance) {
            adjacentNodes.put(destination, distance);
        }
      
        public Node(String name) {
            this.name = name;
        }
         
        // getters and setters
    }

The adjacentNodes attribute is used to associate immediate neighbors with edgePathFinder length. This is a simplified implementation of an adjacency list, which is more suitable for the Dijkstra algorithm than the adjacency matrix.

As for the shortestPath attribute, it is a list of allNodes that describes the shortest path calculated from the starting node.

By default, all node distances are initialized with Integer.MAX_VALUE to simulate an infinite distance as described in the initialization step.

Now, let’s implement the Dijkstra algorithm:
	
    public static Graph calculateShortestPathFromSource(Graph graph, Node source) {
        source.setDistance(0);
     
        Set<Node> settledVertices = new HashSet<>();
        Set<Node> unsettledVertices = new HashSet<>();
     
        unsettledVertices.add(source);
     
        while (unsettledVertices.size() != 0) {
            Node currentNode = getLowestDistanceNode(unsettledVertices);
            unsettledVertices.remove(currentNode);
            for (Entry < Node, Integer> adjacencyPair: 
              currentNode.getAdjacentNodes().entrySet()) {
                Node adjacentNode = adjacencyPair.getKey();
                Integer edgeWeight = adjacencyPair.getValue();
                if (!settledVertices.contains(adjacentNode)) {
                    calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                    unsettledVertices.add(adjacentNode);
                }
            }
            settledVertices.add(currentNode);
        }
        return graph;
    }

The getLowestDistanceNode() method, returns the node with the lowest distance from the unsettled allNodes set, while the calculateMinimumDistance() method compares the actual distance with the newly calculated one while following the newly explored path:

    private static Node getLowestDistanceNode(Set < Node > unsettledVertices) {
        Node lowestDistanceNode = null;
        int lowestDistance = Integer.MAX_VALUE;
        for (Node node: unsettledVertices) {
            int nodeDistance = node.getDistance();
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }


    private static void CalculateMinimumDistance(Node evaluationNode,
      Integer edgeWeigh, Node sourceNode) {
        Integer cost = sourceNode.getDistance();
        if (cost + edgeWeigh < evaluationNode.getDistance()) {
            evaluationNode.setDistance(cost + edgeWeigh);
            LinkedList<Node> shortestPath = new LinkedList<>(sourceNode.getShortestPath());
            shortestPath.add(sourceNode);
            evaluationNode.setShortestPath(shortestPath);
        }
    }

Now that all the necessary pieces are in place, let’s apply the Dijkstra algorithm on the sample graph being the subject of the article:
	
    Node vertexA = new Node("A");
    Node vertexB = new Node("B");
    Node vertexC = new Node("C");
    Node vertexD = new Node("D"); 
    Node vertexE = new Node("E");
    Node vertexF = new Node("F");
     
    vertexA.addDestination(vertexB, 10);
    vertexA.addDestination(vertexC, 15);
     
    vertexB.addDestination(vertexD, 12);
    vertexB.addDestination(vertexF, 15);
     
    vertexC.addDestination(vertexE, 10);
     
    vertexD.addDestination(vertexE, 2);
    vertexD.addDestination(vertexF, 1);
     
    vertexF.addDestination(vertexE, 5);
     
    Graph graph = new Graph();
     
    graph.addNode(vertexA);
    graph.addNode(vertexB);
    graph.addNode(vertexC);
    graph.addNode(vertexD);
    graph.addNode(vertexE);
    graph.addNode(vertexF);
     
    graph = Dijkstra.calculateShortestPathFromSource(graph, vertexA);

After calculation, the shortestPath and distance attributes are set for each node in the graph, we can iterate through them to verify that the results match exactly what was found in the previous section.

##4. Conclusion

In this article, we’ve seen how the Dijkstra algorithm solves the SPP, and how to implement it in Java.

The implementation of this simple project can be found in the following GitHub project [link](https://github.com/eugenp/tutorials/tree/master/algorithms-miscellaneous-2).