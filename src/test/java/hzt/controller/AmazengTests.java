package hzt.controller;

import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(ApplicationExtension.class)
public class AmazengTests {

    @Start
    void start(Stage stage) {
        stage.setMaximized(true);
        new AppManager(stage).start();
    }

    @Test
    void whenAppStartsSolveMethodCanBeSelected(FxRobot robot) throws InterruptedException {
        final var comboBox = robot.lookup("#solve-methods-combobox").queryAs(ComboBox.class);
        final var initialValue = comboBox.getValue();

        robot.clickOn(comboBox).clickOn().type(KeyCode.DOWN).type(KeyCode.DOWN).type(KeyCode.DOWN);

        final var valueAfterSelection = comboBox.getValue();

        assertAll(
                () -> assertEquals("Dijkstra", initialValue),
                () -> assertEquals("A*", valueAfterSelection)
        );
    }
}
