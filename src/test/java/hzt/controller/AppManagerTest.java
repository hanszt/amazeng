package hzt.controller;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

import java.util.function.Consumer;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(ApplicationExtension.class)
class AppManagerTest {

    @Start
    void setup(Stage stage) {
        new AppManager(stage);
        stage.show();
    }

    @Test
    void lookupRootAndAllChildren(FxRobot robot) {
        //arrange
        final var root = robot.lookup("#root").queryAs(StackPane.class);
        final var children = allChildren(root).toList();
        children.forEach(System.out::println);

        //assert: expected, actual
        assertEquals(485, children.size());
    }

    private static Stream<Node> allChildren(Parent parent) {
        final var nodes = Stream.<Node>builder();
        allChildren(parent, nodes);
        return nodes.build();
    }

    private static void allChildren(Parent parent, Consumer<Node> nodeConsumer) {
        for (final var node : parent.getChildrenUnmodifiable()) {
            nodeConsumer.accept(node);
            if (node instanceof Parent p) {
                allChildren(p, nodeConsumer);
            }
        }
    }
}